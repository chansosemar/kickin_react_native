/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import store from './src/redux/store';

import AppStack from './src/navigator/AppStack';
import AfterBook from './src/screens/AfterBook'
import BookPage2 from './src/screens/BookPage2'

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <AppStack/>
    </Provider>
  );
};

export default App;
