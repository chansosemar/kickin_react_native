import {LOGIN, LOGIN_ADMIN ,LOGOUT, LOGOUT_ADMIN, REGISTER,REGISTER_GOOGLE, LOGIN_GOOGLE} from './actionTypes';

export const loginAction = (payload) => {
  return {type: LOGIN, payload};
};

export const loginAdminAction = (payload) => {
  return {type: LOGIN_ADMIN, payload};
};


export const loginGoogleAction = (payload) => {
  return {type: LOGIN_GOOGLE, payload};
};

export const logoutAction = () => {
  return {type: LOGOUT};
};

export const logoutAdminAction = () => {
  return {type: LOGOUT_ADMIN};
};

export const registerAction = (payload) => {
  return {type: REGISTER, payload};
};

export const registerGoogleAction = (payload) => {
  return {type: REGISTER_GOOGLE, payload};
};

