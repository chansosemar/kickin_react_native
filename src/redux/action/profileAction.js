import {GET_PROFILE, EDIT_PROFILE} from './actionTypes';

export const getProfileDetail = () => {
  return {type: GET_PROFILE};
};

export const editProfileDetail = (payload) => {
  return {
    type: EDIT_PROFILE,
    payload,
  };
};
