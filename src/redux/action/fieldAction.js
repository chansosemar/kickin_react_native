import {GET_FIELD, GET_FIELD_DETAIL} from './actionTypes';

export const getFieldAction = (payload) => {
  return {
    type: GET_FIELD,
    payload,
  };
};

export const getFieldDetailAction = (payload) => {
  return {
    type: GET_FIELD_DETAIL,
    payload,
  };
};
