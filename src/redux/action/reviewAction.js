import {GET_REVIEW, GET_USER_REVIEW, EDIT_REVIEW} from './actionTypes';

export const getReviewAction = (payload) => {
  return {
    type: GET_REVIEW,
    payload,
  };
};

export const getUserReviewAction = (payload) => {
  return {
    type: GET_USER_REVIEW,
    payload,
  };
};

export const editReviewAction = (payload) => {
  return {
    type: EDIT_REVIEW,
    payload,
  };
};
