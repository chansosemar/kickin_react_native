import {GET_RENT_LIST, POST_SOCKS, POST_SHOES, POST_VEST} from './actionTypes';

export const getRentalListAction = (payload) => {
  return {
    type: GET_RENT_LIST,
    payload,
  };
};

export const postSocksAction = (payload) => {
  return {
    type: POST_SOCKS,
    payload,
  };
};

export const postShoesAction = (payload) => {
  return {
    type: POST_SHOES,
    payload,
  };
};

export const postVestAction = (payload) => {
  return {
    type: POST_VEST,
    payload,
  };
};