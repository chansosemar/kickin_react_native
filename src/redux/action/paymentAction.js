import {POST_PAYMENT, GET_PAYADM, GET_BOOKADM, PATCH_PAYMENT, PATCH_BOOKING} from './actionTypes';

export const paymentAction = (payload) => {
  return {type: POST_PAYMENT, payload};
};

export const getPaymentAdmin = (payload) => {
  return {type: GET_PAYADM, payload};
};

export const getBookingAdmin = (payload) => {
  return {type: GET_BOOKADM, payload};
};

export const patchPayment = (payload) => {
  return {type: PATCH_PAYMENT, payload};
};

export const patchBooking = (payload) => {
  return {type: PATCH_BOOKING, payload};
};

