import {GET_BOOKING_FIELD, ADD_BOOKING, GET_BOOKING_USER} from './actionTypes';

export const getBookingFieldListAction = (payload) => {
  return {
    type: GET_BOOKING_FIELD,
    payload,
  };
};

export const addBookingAction = (payload) => {
  return {
    type: ADD_BOOKING,
    payload,
  };
};

export const getBookingUserAction = (payload) => {
  return {
    type: GET_BOOKING_USER,
    payload,
  };
};
