import {ADD_TICKET, GET_TICKET} from './actionTypes';

export const addTicketAction = (payload) => {
  return {
    type: ADD_TICKET,
    payload,
  };
};
export const getTicketAction = (payload) => {
  return {
    type: GET_TICKET,
    payload,
  };
};
