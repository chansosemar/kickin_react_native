import {POST_TRX, GET_TRX} from './actionTypes';

export const postTrx = (payload) => {
  return {
    type: POST_TRX,
    payload,
  };
};

export const getTrx = (payload) => {
  return {
    type: GET_TRX,
    payload,
  };
};