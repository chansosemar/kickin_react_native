import {
  GET_FIELD,
  GET_FIELD_SUCCESS,
  GET_FIELD_FAILED,
  GET_FIELD_DETAIL,
  GET_FIELD_DETAIL_SUCCESS,
  GET_FIELD_DETAIL_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const field = (state = intialState, action) => {
  switch (action.type) {
    case GET_FIELD: {
      return {
        ...state,
        isLoading: true,
        // fields:[payload]
      };
    }
    case GET_FIELD_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_FIELD_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default field;
