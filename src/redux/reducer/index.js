import {combineReducers} from 'redux';
import auth from './authReducer';
import profile from './profileReducer';
import field from './fieldReducer';
import fieldDetail from './fieldDetailReducer';
import review from './reviewReducer';
import booking from './addBookingReducer';
import ticket from './ticketReducer';
import getTicket from './getTicketReducer';
import getUserBooking from './getUserBookingReducer';
import getUserReview from './getUserReviewReducer';
import getBookingFieldList from './getBookingFieldListReducer';
import getRentalKitList from './rentalKitReducer';
import transaction from './transactionReducer';
import payment from './paymentReducer';
import getPaymentAdmin from './getPaymentAdmin'
import getBookingAdmin from './getBookingAdmin'
import patchAdmin from './adminReducer'

export default combineReducers({
  auth,
  profile,
  field,
  fieldDetail,
  review,
  booking,
  ticket,
  getTicket,
  getUserBooking,
  getUserReview,
  getBookingFieldList,
  getRentalKitList,
  transaction,
  payment,
  getPaymentAdmin,
  getBookingAdmin,
  patchAdmin

});
