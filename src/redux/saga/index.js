import {all} from 'redux-saga/effects';
import authSaga from './authSaga';
import profileSaga from './profileSaga';
import fieldSaga from './fieldSaga';
import reviewSaga from './reviewSaga';
import bookingSaga from './bookingSaga';
import ticketSaga from './ticketSaga';
import rentalKitSaga from './rentalKitSaga';
import transactionSaga from './transactionSaga';
import paymentSaga from './paymentSaga';
import adminSaga from './adminSaga'

export default function* rootSaga() {
  yield all([
    authSaga(),
    profileSaga(),
    fieldSaga(),
    reviewSaga(),
    bookingSaga(),
    ticketSaga(),
    rentalKitSaga(),
    transactionSaga(),
    paymentSaga(),
    adminSaga()
  ]);
}
