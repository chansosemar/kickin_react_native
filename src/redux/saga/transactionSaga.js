import {takeLatest, put} from 'redux-saga/effects';
import {apiPostTRX,apiGetTRX} from '../../common/api/mainApi';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
	POST_TRX,
	POST_TRX_SUCCESS,
	POST_TRX_FAILED,
	GET_TRX,
	GET_TRX_SUCCESS,
	GET_TRX_FAILED,
} from '../action/actionTypes';

function* postTRXaction(action) {
	try {
		const headers = yield getHeaders();
		console.log(action.payload)
		const resPostTRX = yield apiPostTRX(action.payload.id, headers);

		console.info('ini resPostTRX', resPostTRX.data);
		yield put({type: POST_TRX_SUCCESS});
		console.log('berhasil add TRX');
	} catch (e) {
		console.log('gagal add TRX', JSON.stringify(e));
		yield put({type: POST_TRX_FAILED});
	}
}

function* getTRXaction(action) {
	try {
		const headers = yield getHeaders();
		console.log(action.payload)
		const resGetTRX = yield apiGetTRX(action.payload, headers);

		console.info('ini resGetTRX', resGetTRX.data);
		yield put({type: GET_TRX_SUCCESS, payload: resGetTRX.data});
		console.log('berhasil get TRX');
	} catch (e) {
		console.log('gagal get TRX', JSON.stringify(e));
		yield put({type: GET_TRX_FAILED});
	}
}

function* transactionSaga() {
	yield takeLatest(POST_TRX, postTRXaction);
	yield takeLatest(GET_TRX, getTRXaction);
}

export default transactionSaga;
