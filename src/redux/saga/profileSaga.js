import {takeLatest, put} from 'redux-saga/effects';
import {apiGetProfile, apiEditProfile} from '../../common/api/mainApi';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED,
  EDIT_PROFILE,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_FAILED,
} from '../action/actionTypes';

function* getProfileDetail() {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resProfile = yield apiGetProfile(accountId, headers);
    console.info('ini resprofile', resProfile.data);
    yield put({type: GET_PROFILE_SUCCESS, payload: resProfile.data});
    console.log('berhasil ambil data profile');
  } catch (e) {
    ToastAndroid.showWithGravity(
      'Gagal mengambil data profil',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );

    yield put({type: GET_PROFILE_FAILED});
  }
}

function* editProfileDetail(action) {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resProfile = yield apiEditProfile(accountId, headers, action.payload);
    if (resProfile && resProfile.data) {
      console.log('ahoooy', resProfile.data);
    } else {
      console.log('tidak ada');
    }

    console.info('ini resprofile', resProfile.data);
    yield put({type: EDIT_PROFILE_SUCCESS, payload: resProfile.data});
    console.log('berhasil edit profile');
    yield put({type: GET_PROFILE});
  } catch (e) {
    ToastAndroid.showWithGravity(
      'Gagal mengambil edit profil',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );

    yield put({type: EDIT_PROFILE_FAILED});
  }
}

function* profileSaga() {
  yield takeLatest(GET_PROFILE, getProfileDetail);
  yield takeLatest(EDIT_PROFILE, editProfileDetail);
}

export default profileSaga;
