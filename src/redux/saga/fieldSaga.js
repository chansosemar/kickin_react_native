import {takeLatest, put} from 'redux-saga/effects';
import {apiGetAllField,apiGetAllFieldDetail} from '../../common/api/mainApi';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_FIELD,
  GET_FIELD_SUCCESS,
  GET_FIELD_FAILED,
  GET_FIELD_DETAIL,
  GET_FIELD_DETAIL_FAILED,
  GET_FIELD_DETAIL_SUCCESS,
} from '../action/actionTypes';

function* getFieldAction() {
  try {
    const headers = yield getHeaders();
    // const accountId = yield getAccountId();

    const resField = yield apiGetAllField(headers);
    console.log(resField)

    yield put({
      type: GET_FIELD_SUCCESS,
      payload: resField.data,
    });
    console.log('Berhasil mengambil data Field');
  } catch (e) {
    ToastAndroid.showWithGravity(
      'gagal mengambil data Field',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
    yield put({
      type: GET_FIELD_FAILED,
    });
  }
}

function* getFieldDetailAction(action) {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();
    console.log('1')
    console.log('action payload', action.payload)
    const resFieldDetail = yield apiGetAllFieldDetail(action.payload , headers);

    console.info('ini resFieldDetail', resFieldDetail.data);
    yield put({type: GET_FIELD_DETAIL_SUCCESS, payload: resFieldDetail.data});
    console.log('berhasil ambil field detail');
  } catch (e) {
    console.log('gagal ambil field detail',e)

    yield put({type: GET_FIELD_DETAIL_FAILED});
  }
} 

function* fieldSaga() {
  yield takeLatest(GET_FIELD, getFieldAction);
  yield takeLatest(GET_FIELD_DETAIL, getFieldDetailAction);
}

export default fieldSaga;
