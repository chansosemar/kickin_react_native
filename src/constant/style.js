// DIMENSIONS
import {Dimensions} from 'react-native';
export const DIMENSHEIGHT = Dimensions.get('window').height;
export const DIMENSWIDTH = Dimensions.get('window').width;

// LANDING PAGE

export const TXTMAINBTN = {
	color: '#222',
	fontFamily: 'Montserrat-Bold',
	fontSize: 18,
};
export const SPCTXT = {
	color: '#FFF',
	fontFamily: 'Montserrat-Light',
	fontSize: 12,
	textAlign: 'center',
};
export const TXTSNDBTN = {
	color: '#fff',
	fontFamily: 'Montserrat-Bold',
	fontSize: 18,
};
export const MAINBTN = {
	height: 60,
	width: DIMENSWIDTH * 0.8,
	backgroundColor: '#53C9C2',
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 50,
	marginVertical: 5,
	alignSelf: 'center',
};
export const SNDMAINBTN = {
	height: 60,
	width: DIMENSWIDTH * 0.8,
	alignItems: 'center',
	justifyContent: 'center',
	borderWidth: 1,
	borderColor: '#545454',
	borderRadius: 50,
	marginVertical: 5,
};

export const LOGO = {
	color: '#53C9C2',
	fontSize: 30,
	fontFamily: 'Montserrat-ExtraBoldItalic',
};

// LOGINPAGE
export const CONTAINER = {
	backgroundColor: '#222',
	height: DIMENSHEIGHT,
	alignItems: 'center',
};

export const CONTENT = {
	width: DIMENSWIDTH * 0.8,
	paddingVertical: 20,
	alignItems: 'center',
};
export const SMALLLOGO = {
	color: '#53C9C2',
	fontSize: 20,
	fontFamily: 'Montserrat-ExtraBoldItalic',
	alignSelf: 'center',
};
export const WARNTEXT = {
	color: '#D72222',
	fontFamily: 'Montserrat-Regular',
	padding: 10,
	fontSize: 14,
};
export const FORMINPUT = {
	width: DIMENSWIDTH * 0.9,
	borderColor: '#545454',
	borderWidth: 1,
	borderRadius: 50,
	color: '#fff',
	marginVertical: 5,
	padding: 10,
};
export const LABELFONT = {
	fontFamily: 'Montserrat-Light',
	fontSize: 16,
	color: '#fff',
};

export const SHOWPASSWORD = {
	position: 'absolute',
	right: 20,
	top: 22,
};
export const TXTGOOGLEBTN = {
	color: '#FFF',
	fontFamily: 'Montserrat-Regular',
	fontSize: 18,
	right: 20,
};

export const GOOGLEBTN = {
	alignSelf: 'center',
	height: 60,
	width: DIMENSWIDTH * 0.8,
	borderColor: '#D72222',
	borderWidth: 1,
	alignItems: 'center',
	borderRadius: 50,
	marginVertical: 5,
	display: 'flex',
	flexDirection: 'row',
	justifyContent: 'space-around',
};

// REGISTER PAGE

export const THINFONT = {
	fontFamily: 'Montserrat-ThinItalic',
	fontSize: 12,
	color: '#fff',
};

// HOME PAGE

export const CARDLIST = {
	width: (DIMENSWIDTH * 0.9) / 2,
	margin: 5,
	borderWidth: 1,
	borderColor: '#53C9C2',
	borderRadius: 10,
	padding: 5,
};

export const IMAGELIST = {
	height: 150,
	borderRadius: 10,
};

export const CONTGRADIENT = {
	position: 'absolute',
	width: '100%',
	height: 150,
};

export const CAROUSELTXTSMALL = {
	fontFamily: 'Montserrat-Regular',
	top: 85,
	fontSize: 16,
	color: '#fff',
	textAlign: 'right',
	padding: 15,
};

export const CAROUSELIMG = {
	width: DIMENSWIDTH,
	height: 120,
};

export const HEADERTXT = {
	fontFamily: 'Montserrat-Medium',
	fontSize: 15,
	color: '#fff',
	marginBottom: 10,
	marginLeft: 10,
	fontSize: 20,
	alignSelf: 'flex-start',
};

export const SORTBTN = {
	alignSelf: 'center',
	height: 30,
	width: DIMENSWIDTH * 0.4,
	borderColor: '#53C9C2',
	borderWidth: 1,
	alignItems: 'center',
	borderRadius: 50,
	marginBottom: 10,
	marginHorizontal: 10,
	justifyContent: 'center',
};

export const TXTSORTBTN = {
	color: '#53C9C2',
	fontFamily: 'Montserrat-Regular',
	fontSize: 12,
};

export const SORTACTIVEBTN = {
	alignSelf: 'center',
	height: 30,
	width: DIMENSWIDTH * 0.4,
	backgroundColor: '#53C9C2',
	alignItems: 'center',
	borderRadius: 50,
	marginBottom: 10,
	marginHorizontal: 10,
	justifyContent: 'center',
};

export const TXTSORTACTIVEBTN = {
	color: '#222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 12,
};

// PROFILE PAGE

export const PROFILEIMG = {
	width: DIMENSWIDTH,
	height: 200,
};

export const PROFILEGRADIENT = {
	position: 'absolute',
	width: '100%',
	height: 200,
};

export const HEADERFONT = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 15,
	color: '#fff',
	fontSize: 18,
	textAlign: 'right',
	marginHorizontal: 20,
	top: 80,
};
export const CONTENTPROFILE = {
	width: DIMENSWIDTH * 0.9,
	paddingVertical: 20,
	alignItems: 'center',
};

export const LABELFONTPROFILE = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
	color: '#53C9C2',
	marginRight: 20,
};

export const REGFONTPROFILE = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
	color: '#FFF',
	textAlign: 'justify',
};

export const CARDREVIEW = {
	width: '100%',
	borderColor: '#545454',
	borderWidth: 1,
	borderRadius: 20,
	padding: 20,
	marginVertical: 10,
};

export const TITLEREVIEW = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
	color: '#fff',
};
export const TXTREVIEW = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 14,
	color: '#fff',
};

export const MODALCARD = {
	width: '100%',
	backgroundColor: '#222',
	borderWidth: 2,
	borderColor: '#fff',
	borderRadius: 30,
	padding: 20,
};

export const TXTEDITBTN = {
	color: '#222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
};

export const EDITBTN = {
	height: 40,
	width: DIMENSWIDTH * 0.4,
	backgroundColor: '#53C9C2',
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 50,
	alignSelf: 'center',
};

export const TXTLGTBTN = {
	color: '#D72222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
};

export const LGTBTN = {
	height: 40,
	width: DIMENSWIDTH * 0.4,
	borderColor: '#D72222',
	borderWidth: 1,
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 50,
	alignSelf: 'center',
};

// EDIT PROFILE PAGE

export const EDITPICTUREBTN = {
	alignSelf: 'center',
	fontFamily: 'Montserrat-Medium',
	color: '#fff',
	borderColor: '#53C9C2',
	borderWidth: 1,
	borderRadius: 10,
	paddingVertical: 2,
	paddingHorizontal: 8,
	marginBottom: 20,
	fontSize: 16,
};

export const MODALASK = {
	width: DIMENSWIDTH * 0.5,
	height: 150,
	margin: 10,
	backgroundColor: '#222',
	borderWidth: 2,
	borderColor: '#fff',
	borderRadius: 20,
	padding: 10,
	alignItems: 'center',
};

export const YESBTN = {
	backgroundColor: '#53C9C2',
	width: ((DIMENSWIDTH * 0.8) / 2) * 0.9,
	height: 35,
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 20,
	marginVertical: 5,
};

export const TXTYESBTN = {
	color: '#222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 18,
};

export const NOBTN = {
	borderColor: '#D72222',
	borderWidth: 1,
	width: ((DIMENSWIDTH * 0.8) / 2) * 0.9,
	height: 35,
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 20,
	marginVertical: 5,
};

export const TXTNOBTN = {
	color: '#FFF',
	fontFamily: 'Montserrat-Regular',
	fontSize: 18,
};

// BOOKPAGE

export const FIELDNAME = {
	color: '#fff',
	fontFamily: 'Montserrat-Regular',
	fontSize: 18,
	borderWidth: 1,
	borderColor: '#53C9C2',
	alignSelf: 'center',
	backgroundColor: '#222',
	paddingVertical: 10,
	paddingHorizontal: 20,
	borderRadius: 50,
	marginVertical: 5,
};

export const LABELFONTBOOK = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 20,
	color: '#53C9C2',
};

// CHECKOUT
export const CONTENTCHECK = {
	width: DIMENSWIDTH * 0.9,
	paddingVertical: 20,
};
export const HEADERFONTCHECK = {
	fontFamily: 'Montserrat-Bold',
	fontSize: 18,
	color: '#53C9C2',
};

export const LABELFONTCHECK = {
	fontFamily: 'Montserrat-Bold',
	fontSize: 16,
	color: '#53C9C2',
};

// RECEIPT PAGE

export const TXTLATERBTN = {
	color: '#D72222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 18,
};

// RENTAL KIT

export const FORMINPUTRENTAL = {
	width: DIMENSWIDTH * 0.2,
	borderColor: '#545454',
	borderWidth: 1,
	borderRadius: 50,
	color: '#fff',
	marginVertical: 5,
	marginHorizontal: 5,
	padding: 10,
};
export const CONFIRMBTN = {
	height: 30,
	width: DIMENSWIDTH * 0.5,
	backgroundColor: '#53C9C2',
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 50,
	marginVertical: 5,
	alignSelf: 'center',
};
export const TXTCONFIRMBTN = {
	color: '#222',
	fontFamily: 'Montserrat-Regular',
	fontSize: 16,
};
export const SKIPBTN = {
	height: 60,
	width: DIMENSWIDTH * 0.8,
	alignSelf: 'center',
	alignItems: 'center',
	justifyContent: 'center',
	borderWidth: 1,
	borderColor: '#545454',
	borderRadius: 50,
	marginVertical: 5,
};

// FIELD DETAIL

export const FIELDDETAILIMG = {
	width: DIMENSWIDTH,
	height: 200,
};
export const CONTENTFIELDDETAIL = {
	width: DIMENSWIDTH * 0.9,
	paddingVertical: 20,
	alignItems: 'center',
};
export const FIELDDETAILBTN = {
	height: 60,
	width: DIMENSWIDTH * 0.8,
	backgroundColor: '#53C9C2',
	alignItems: 'center',
	justifyContent: 'center',
	borderRadius: 50,
	marginVertical: 5,
	alignSelf: 'center',
};
export const FLEXCONT = {
	width: '100%',
	display: 'flex',
	flexDirection: 'row',
	justifyContent: 'space-between',
};
export const BOLDXLFONT = {fontFamily: 'Montserrat-Bold', fontSize: 24};
export const BOLDFONT = {fontFamily: 'Montserrat-Bold', fontSize: 15};
export const THINXLFONT = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 14,
	textAlign: 'justify',
};
export const REGFONT = {
	fontFamily: 'Montserrat-Regular',
	fontSize: 14,
	color: '#fff',
	textAlign: 'justify',
};
export const MEDFONT = {
	fontFamily: 'Montserrat-Medium',
	fontSize: 15,
	color: '#fff',
};


// HISTORY PAGE

export const STATUSFONT = {fontFamily: 'Montserrat-Bold', fontSize: 15, textTransform: 'uppercase', color:'#222'};

export const STATUSFONTDCN = {fontFamily: 'Montserrat-Bold', fontSize: 15, textTransform: 'uppercase', color:'#FFF'};

export const STSBTNACPT = {
  width: DIMENSWIDTH * 0.9,
  height: 50,
  backgroundColor: '#5CDB5C',
  marginVertical: 10,
  borderRadius: 50,
  alignItems: 'center',
  justifyContent: 'center',
};

export const STSBTNDCN = {
  width: DIMENSWIDTH * 0.9,
  height: 50,
  backgroundColor: '#D72222',
  marginVertical: 10,
  borderRadius: 50,
  alignItems: 'center',
  justifyContent: 'center',
};

export const STSBTNWFP = {
  width: DIMENSWIDTH * 0.9,
  height: 50,
  backgroundColor: '#53C9C2',
  marginVertical: 10,
  borderRadius: 50,
  alignItems: 'center',
  justifyContent: 'center',
};
export const CONTENTTICKET = {
	width: DIMENSWIDTH * 0.9,
	alignItems: 'center',
};

export const BOOKCODE = {
	fontFamily: 'Montserrat-Bold',
	fontSize: 30,
	color: '#53C9C2',
};

export const DIRECTIONBTN = {
  width: DIMENSWIDTH * 0.7,
  backgroundColor: '#7b7b7b',
  alignItems: 'center',
  padding: 10,
  borderRadius: 10,
  margin: 10,
};