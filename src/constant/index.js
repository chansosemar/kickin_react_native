// DIMENSIONS
import {Dimensions} from 'react-native';
export const DIMENSHEIGHT = Dimensions.get('window').height;
export const DIMENSWIDTH = Dimensions.get('window').width;

// CONTAINER
export const CONTAINER = {
  backgroundColor: '#222',
  height: DIMENSHEIGHT,
  alignItems: 'center',
};
export const CONTENT = {
  width: DIMENSWIDTH * 0.9,
  paddingVertical: 20,
  alignItems: 'center',
};
export const CONTCAROUSEL = {
  flexDirection: 'row',
  justifyContent: 'center',
  width: '100%',
};
export const CONTCAROUSELXL = {
  flexDirection: 'row',
  justifyContent: 'center',
  width: DIMENSWIDTH,
};

export const FLEXCONT = {
  width: '100%',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
};

export const CARDREVIEW = {
  width: '100%',
  backgroundColor: '#313131',
  borderRadius: 5,
  padding: 20,
  marginVertical: 10,
};

export const PRICECONT = {
  alignSelf: 'center',
  fontFamily: 'Montserrat-Medium',
  color: '#222',
  borderRadius: 10,
  paddingVertical: 2,
  paddingHorizontal: 8,
  marginTop: 10,
  fontSize: 20,
  backgroundColor: '#53C9C2',
};

export const MODALCONT = {
  width: DIMENSWIDTH * 0.5,
  height: 150,
  margin: 10,
  backgroundColor: '#313131',
  borderRadius: 5,
  padding: 10,
  alignItems: 'center',
};

export const EDITPICTUREBTN = {
  alignSelf: 'center',
  fontFamily: 'Montserrat-Medium',
  color: '#fff',
  borderColor: '#53C9C2',
  borderWidth: 1,
  borderRadius: 10,
  paddingVertical: 2,
  paddingHorizontal: 8,
  marginBottom: 20,
  fontSize: 16,
};

export const WRAPPER = {
  position: 'absolute',
  bottom: 200,
  justifyContent: 'center',
  alignItems: 'center',
  marginVertical: 30,
  padding: 20,
};

export const WRAPPERSCND = {
  position: 'absolute',
  bottom: 50,
  justifyContent: 'center',
  alignItems: 'center',
  marginVertical: 30,
  padding: 20,
};

export const PAGIWRAPPER = {
  position: 'absolute',
  bottom: 50,
  left: 0,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
};

export const PAGIDOTS = {
  height: 10,
  width: 10,
  borderRadius: 10 / 2,
  backgroundColor: '#53C9C2',
  marginLeft: 10,
};

// LOGO
export const LOGO = {
  color: '#53C9C2',
  fontSize: 35,
  fontFamily: 'Montserrat-ExtraBoldItalic',
};
export const SMALLLOGO = {
  color: '#53C9C2',
  fontSize: 18,
  fontFamily: 'Montserrat-ExtraBoldItalic',
};

// FONT
export const MEDFONT = {fontFamily: 'Montserrat-Medium', fontSize: 15};
export const BOLDFONT = {fontFamily: 'Montserrat-Bold', fontSize: 15};
export const BOLDXLFONT = {fontFamily: 'Montserrat-Bold', fontSize: 24};
export const THINFONT = {fontFamily: 'Montserrat-ThinItalic', fontSize: 12};
export const THINXLFONT = {
  fontFamily: 'Montserrat-Regular',
  fontSize: 14,
  textAlign: 'justify',
};
export const BOLDBLUEFONT = {
  fontFamily: 'Montserrat-Bold',
  fontSize: 24,
  color: '#fff',
  alignSelf: 'center',
};
export const REGFONT = {
  fontFamily: 'Montserrat-Regular',
  fontSize: 14,
  color: '#fff',
  textAlign: 'justify',
};

export const LABELFONT = {
  fontFamily: 'Montserrat-Bold',
  fontSize: 18,
  color: '#53C9C2',
  marginVertical: 5,
};
export const CONTENTFONT = {
  fontFamily: 'Montserrat-Regular',
  color: '#fff',
  fontSize: 15,
  textAlign: 'justify',
  marginVertical: 2,
};

export const DATEFONT = {
  fontFamily: 'Montserrat-Regular',
  color: '#fff',
  fontSize: 16,
  backgroundColor: '#313131',
  padding: 10,
  borderRadius: 10,
  marginBottom: 20,
};

export const SELECTFONT = {
  fontFamily: 'Montserrat-Medium',
  color: '#53C9C2',
  fontSize: 16,
};
export const SELECTDURATIONFONT = {
  fontFamily: 'Montserrat-Medium',
  color: '#222',
  fontSize: 16,
};

export const HEADERFONT = {
  fontSize: 30,
  marginBottom: 20,
  fontFamily: 'Montserrat-Bold',
  color: '#53C9C2',
};

export const PARAGRAPHFONT = {
  fontSize: 14,
  textAlign: 'center',
  fontFamily: 'Montserrat-Regular',
  color: '#fff',
};

// MAIN BUTTON
export const TXTMAINBUTTON = {
  color: '#313131',
  fontFamily: 'Montserrat-Bold',
  fontSize: 18,
};
export const MAINBUTTON = {
  height: 60,
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#53C9C2',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 5,
  marginVertical: 5,
};

export const TXTPICKBUTTON = {
  color: '#222',
  fontFamily: 'Montserrat-Bold',
  fontSize: 16,
};
export const PICKBUTTON = {
  height: 40,
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#53C9C2',
  alignSelf: 'center',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 5,
  marginVertical: 5,
};

// SECONDARY BUTTON
export const TXTSECONDBUTTON = {
  color: '#53C9C2',
  fontFamily: 'Montserrat-Bold',
  fontSize: 18,
};
export const SECONDBUTTON = {
  height: 60,
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#545454',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 5,
  marginVertical: 5,
};

//EDIT PROFILE BUTTON
export const TXTEDITBUTTON = {
  alignSelf: 'center',
  fontFamily: 'Montserrat-Medium',
  color: '#53C9C2',
  borderRadius: 5,
  paddingHorizontal: 10,
  paddingVertical: 5,
  marginBottom: 20,
  fontSize: 16,
};

// LOG OUT BUTTON
export const TXTLOGOUTBUTTON = {
  alignSelf: 'center',
  fontFamily: 'Montserrat-Medium',
  color: '#D72222',
  borderRadius: 5,
  paddingHorizontal: 10,
  paddingVertical: 5,
  marginBottom: 20,
  fontSize: 16,
};

//FB BUTTON
export const TEXTFBBUTTON = {
  color: '#fff',
  fontFamily: 'Montserrat-Bold',
  fontSize: 18,
  right: 20,
};

export const FBBUTTON = {
  height: 60,
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#D72222',
  alignItems: 'center',
  borderRadius: 5,
  marginVertical: 5,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
};

//FORM INPUT
export const FORMINPUT = {
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#545454',
  borderRadius: 5,
  color: '#fff',
  marginVertical: 5,
  padding: 10,
};

export const FORMSMALLINPUT = {
  width: DIMENSWIDTH * 0.5,
  color: '#fff',
};

export const FORMXLINPUT = {
  width: DIMENSWIDTH * 0.9,
  height: 100,
  borderRadius: 5,
  color: '#fff',
  marginVertical: 5,
  padding: 10,
};

// CARD BUTTON
export const VIEWBTN = {
  backgroundColor: '#7b7b7b',
  width: ((DIMENSWIDTH * 0.8) / 2) * 0.9,
  height: 35,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 10,
  marginVertical: 5,
};
export const BOOKBTN = {
  backgroundColor: '#53C9C2',
  width: ((DIMENSWIDTH * 0.8) / 2) * 0.9,
  height: 35,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 10,
  marginVertical: 5,
};

// DURATION BUTTON
export const DURATIONBTN = {
  backgroundColor: '#313131',
  width: 50,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 10,
};

export const PICKDURATIONBTN = {
  backgroundColor: '#53C9C2',
  width: 50,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 10,
};
// CAROUSEL
export const CAROUSELIMG = {
  width: DIMENSWIDTH,
  height: 100,
};
export const CAROUSELBTN = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  position: 'relative',
  bottom: 75,
  marginHorizontal: 20,
};
export const CAROUSELIMGXL = {
  width: DIMENSWIDTH,
  height: 200,
};

export const CAROUSELTXT = {
  fontFamily: 'Montserrat-Bold',
  top: 140,
  fontSize: 30,
  color: '#fff',
  textAlign: 'right',
  marginRight: 20,
};

export const CAROUSELTXTSMALL = {
  fontFamily: 'Montserrat-Medium',
  top: 100,
  fontSize: 20,
  color: '#fff',
  textAlign: 'right',
  padding: 15,
};

export const SEARCHTXTSMALL = {
  fontFamily: 'Montserrat-Medium',
  top: 60,
  fontSize: 20,
  color: '#fff',
  textAlign: 'right',
  padding: 15,
};

// AVATAR
export const AVATAR = {
  width: 100,
  height: 100,
  borderRadius: 100,
  borderWidth: 3,
  borderColor: '#53C9C2',
  marginBottom: 10,
};

// WARNING TEXT

export const WARNTEXT = {
  color: '#D72222',
  fontFamily: 'Montserrat-Regular',
  padding: 10,
  fontSize: 14,
};

// SHOW AND HIDE BUTTON

export const SHOWPASSWORD = {
  position: 'absolute',
  right: 20,
  top: 22,
};

// HISTORY CARD
export const HISTORYCARD = {
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#313131',
  marginTop: 10,
  borderRadius: 10,
  padding: 10,
};
export const NEWHISTORYCARD = {
  width: DIMENSWIDTH * 0.9,
  backgroundColor: '#313131',
  marginTop: 10,
  borderRadius: 10,
  padding: 10,
};
export const DIRECTIONBTN = {
  width: DIMENSWIDTH * 0.8,
  backgroundColor: '#7b7b7b',
  alignItems: 'center',
  padding: 10,
  borderRadius: 10,
  margin: 10,
};
export const TICKETBTN = {
  width: DIMENSWIDTH * 0.9,
  height: 50,
  backgroundColor: '#53C9C2',
  marginVertical: 10,
  borderRadius: 10,
  alignItems: 'center',
  justifyContent: 'center',
};
