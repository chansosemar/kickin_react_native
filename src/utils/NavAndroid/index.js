import { Dimensions, StatusBar } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';

const screenHeight = Dimensions.get('screen').height
const windowHeight = Dimensions.get('window').height

const softMenuHeight = screenHeight - windowHeight - StatusBar.currentHeight;
const hasSoftMenu = softMenuHeight ? true : false
const hasHardMenu = ExtraDimensions.isSoftMenuBarEnabled();

export {
    softMenuHeight,
    hasSoftMenu,
    hasHardMenu
}