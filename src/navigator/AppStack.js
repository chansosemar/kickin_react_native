import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text, View, Easing, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {logoutAction} from '../redux/action/authAction';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

import MainNavigator from './MainNavigator';
import EditProfilePage from '../screens/EditProfilePage';
import LandingPage from '../screens/LandingPage';
import LoginPage from '../screens/LoginPage';
import RegisterPage from '../screens/RegisterPage';
import FieldDetailPage from '../screens/FieldDetailPage';
import ProfilePage from '../screens/ProfilePage';
import TicketPage from '../screens/TicketPage';
import BookPage from '../screens/BookPage';
import AfterBook from '../screens/AfterBook';
import SearchPage from '../screens/SearchPage';
import RentalKitPage from '../screens/RentalKitPage';
import SplashScreenLoading from '../screens/SplashScreen';
import CheckoutPage from '../screens/CheckoutPage';
import UploadReceiptPage from '../screens/UploadReceiptPage';
import AdminPage from '../screens/AdminPage';

const Stack = createStackNavigator();

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 100,
    mass: 3,
    overshootClamping: false,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const closeConfig = {
  animation: 'timing',
  config: {
    duration: 250,
    easing: Easing.linear,
  },
};

const AppStack = (props) => {
  const statusLogin = useSelector((state) => state.auth.isLoggedIn);
  const statusAdmin = useSelector((state) => state.auth.isAdmin);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    SplashScreen.hide();
    checkLogin();
  }, []);

  const checkLogin = async () => {
    const token = await AsyncStorage.getItem('APP_AUTH_TOKEN');
    console.log(token);
    if (token) {
      dispatch({type: 'LOGIN_SUCCESS'});
      dispatch({type: 'GET_PROFILE'});
      dispatch({type: 'GET_FIELD'});
      dispatch({type: 'GET_BOOKING_USER'});
      dispatch({type: 'GET_USER_REVIEW'});
      dispatch({type: 'GET_RENT_LIST'});
    }
    setIsLoading(false);
  };

  if (isLoading) {
    return <SplashScreenLoading />;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          transitionSpec: {
            open: config,
            close: closeConfig,
          },
        }}
        animation="fade">
        {statusAdmin ? (
          <Stack.Screen
            name="Admin"
            component={AdminPage}
            options={{headerShown: false}}
          />
        ) : statusLogin ? (
          <>
            <Stack.Screen
              name="Main"
              component={MainNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Edit Profile"
              component={EditProfilePage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Rental Kit"
              component={RentalKitPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="AfterBook"
              component={AfterBook}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Checkout"
              component={CheckoutPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Upload Receipt"
              component={UploadReceiptPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Search"
              component={SearchPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Ticket"
              component={TicketPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Book"
              component={BookPage}
              options={{headerShown: false}}
            />

            <Stack.Screen
              name="Field Details"
              component={FieldDetailPage}
              options={{headerShown: false}}
            />
          </>
        ) : (
          <>
            <Stack.Screen
              options={{headerShown: false}}
              name="LandingPage"
              component={LandingPage}
            />
            <Stack.Screen
              name="Home"
              component={MainNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Field Details"
              component={FieldDetailPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Login"
              component={LoginPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Register"
              component={RegisterPage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Search"
              component={SearchPage}
              options={{headerShown: false}}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppStack;
