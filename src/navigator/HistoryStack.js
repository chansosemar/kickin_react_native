import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text, View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';


import HistoryPage from '../screens/HistoryPage';

const Stack = createStackNavigator();
 
const HistoryStack = (props) => {
  const navigation = useNavigation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="History"
        component={HistoryPage}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default HistoryStack;
