import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/FontAwesome';
import * as G from '../config/style';

import HomePage from '../screens/HomePage';
import SearchPage from '../screens/SearchPage';
import HistoryPage from '../screens/HistoryPage';
import ReviewPage from '../screens/ReviewPage';
import ProfilePage from '../screens/ProfilePage'

import {Text, StyleSheet} from 'react-native';
import {wp, fp, hp} from '../utils/Responsive';

const Tab = createBottomTabNavigator();
const {fontSize, fontColor, font, color} = G.AppStyle;

const MainNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName={'Home'}
      tabBarOptions={{
        showLabel: false,
        activeTintColor: color.primary,
        inactiveTintColor: color.disable,
        style: {
          backgroundColor: color.dark,
          height: hp(10),
          width: wp(100),
          borderTopWidth: wp(0),
          
        },
        tabStyle: {
          paddingVertical: hp(1),

        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'History') {
            iconName = 'book';
          } else if (route.name === 'Profile') {
            iconName = 'account-circle';
          } else if (route.name === 'Search') {
            iconName = 'search';
          } else if (route.name === 'Review') {
            iconName = 'comment';
          }
          return (
            <Icon
              name={iconName}
              size={fontSize.extraLarge}
              color={color}
              style={{alignSelf: 'center'}}
            />
          );
        },
      })}>
      <Tab.Screen name="Home" component={HomePage} />
      <Tab.Screen name="Search" component={SearchPage} />
      <Tab.Screen
        name="History"
        component={HistoryPage}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Icon name="bookmark" size={fp(7)} color={color} style={styles.ICON} />
          ),
        }}
      />
       <Tab.Screen name="Review" component={ReviewPage} />
      <Tab.Screen name="Profile" component={ProfilePage} />
    </Tab.Navigator>
  );
};

export default MainNavigator;

const styles = StyleSheet.create({
  ICON: {
    position: 'absolute',
    bottom: hp(1.5),
    backgroundColor: color.dark,
    padding: hp(1.5),
    borderRadius: 50,
    borderWidth: fp(0.2),
    borderColor:color.primary,
  },
});
