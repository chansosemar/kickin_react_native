import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text, View} from 'react-native';

import EditProfilePage from '../screens/EditProfilePage';

const Stack = createStackNavigator();

const EditProfileStack = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Edit Profile"
        component={EditProfilePage}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default EditProfileStack;
