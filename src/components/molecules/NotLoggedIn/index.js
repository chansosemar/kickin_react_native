import React from 'react';
import {AppStyle} from '../../../config/style';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import * as G from '../../../constant/style';
import {wp, fp, hp} from '../../../utils/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../atoms/Button'

const {fontSize, fontColor, font, color} = AppStyle;

const NotLoggedIn = ({text, onPress, typeButton, style, navigation}) => {
	return (
		<View style={{backgroundColor: color.dark}}>
			<View style={styles.CONTPOSITION}>
				<View style={styles.TEXT}>
					<Text style={styles.TEXTBUTTON}>
						You are not Logged In
					</Text>
				</View>
				<Button
					onPress={() => navigation.navigate('Login')}
					text="Log in"
					typeButton="PRIMARY"
				/>
				<View style={styles.TEXT}>
					<Button
						text="Don't have an account? Sign Up"
						typeButton="TEXT"
						onPress={() => navigation.navigate('Register')}
					/>
				</View>
			</View>
		</View>
	);
};

export default NotLoggedIn;

const styles = StyleSheet.create({
	CONTPOSITION: {
		position: 'relative',
		top: hp(20),
		height: hp(100),
		alignSelf: 'center',
	},
	TEXT: {
		marginVertical: hp(2),
		alignSelf: 'center',
	},
	TEXTBUTTON: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.primary,
	},
});
