import React from 'react';
import {AppStyle} from '../../../config/style';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import * as G from '../../../constant/style';
import {wp, fp, hp} from '../../../utils/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../atoms/Button';

const {fontSize, fontColor, font, color} = AppStyle;

const NotBookingYet = ({text, onPress, typeButton, style}) => {
	return (
		<View style={{backgroundColor: color.dark}}>
			<View style={styles.CONTPOSITION}>
				<View style={styles.TEXT}>
					<Text style={styles.TEXTBUTTON}>
						You don't have any booking yet
					</Text>
				</View>
				<TouchableOpacity
					onPress={() => navigation.navigate('Home')}
					style={G.MAINBTN}>
					<Text style={G.TXTMAINBTN}>Browse Fields</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default NotBookingYet;

const styles = StyleSheet.create({
	CONTPOSITION: {
		position: 'relative',
		top: hp(20),
		height: hp(100),
		alignSelf: 'center',
	},
	TEXT: {
		marginVertical: hp(2),
		alignSelf: 'center',
	},
	TEXTBUTTON: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.primary,
	},
});
