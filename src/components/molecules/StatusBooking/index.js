import React from 'react';
import * as G from '../../../config/style';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import {wp, fp, hp} from '../../../utils/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../atoms/Button';
import ButtonStatus from '../../atoms/ButtonStatus';
import ButtonFeedback from '../../atoms/ButtonFeedback';
import {
	convertDateToString,
	convertTimeToString,
	checkDate,
} from '../../../common/function/allFunction';
import moment from 'moment';

const {fontSize, fontColor, font, color} = G.AppStyle;

const StatusBooking = ({
	text,
	onPress,
	typeButton,
	style,
	item,
	navigation,
}) => {
	return (
		<View style={styles.CONTREVIEW} key={item.id}>
			<View style={G.CARDREVIEW}>
				<View style={styles.HEADER}>
					<Text style={G.BOOKTITLE}>{item.fieldTime.field.name} </Text>
					<Text style={G.BOOKTITLE}>
						{moment(item.startTime).format('Do MMM YY')}{' '}
					</Text>
				</View>
				<ButtonFeedback
					checkDate={checkDate(item.startTime)}
					status={item.status}
					item={item}
					navigation={navigation}
				/>
			</View>
			<ButtonStatus
				status={item.status}
				text={item.status == 'accepted' ? 'Ticket' : item.status}
				item={item}
				navigation={navigation}
			/>
		</View>
	);
};

export default StatusBooking;

const styles = StyleSheet.create({
	CONTPOSITION: {
		position: 'relative',
		top: hp(20),
		height: hp(100),
		alignSelf: 'center',
	},
	TEXT: {
		marginVertical: hp(2),
		alignSelf: 'center',
	},
	TEXTBUTTON: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.primary,
	},
	HEADER: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	CONTREVIEW: {
		width: wp(90),
		alignItems: 'center',
	},
});
