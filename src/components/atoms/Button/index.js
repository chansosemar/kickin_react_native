import React from 'react';
import {AppStyle} from '../../../config/style';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import * as G from '../../../constant/style';
import {wp, fp, hp} from '../../../utils/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';

const {fontSize, fontColor, font, color} = AppStyle;

const Button = ({text, onPress, typeButton, style}) => {
	return (
		<TouchableOpacity
			style={[
				typeButton == 'PRIMARY'
					? styles.PRIMARY
					: typeButton == 'SECOND'
					? styles.SECOND
					: typeButton == 'GOOGLE'
					? styles.GOOGLE
					: null,
			]}
			onPress={onPress}>
			{typeButton == 'PRIMARY'
				? PRIMARY(text)
				: typeButton == 'SECOND'
				? SECOND(text)
				: typeButton == 'GOOGLE'
				? GOOGLE(text)
				: TEXT(text)}
		</TouchableOpacity>
	);
};

export default Button;

const PRIMARY = (text, style) => {
	return (
		<Text style={styles.TEXTPRIMARY} numberOfLines={1}>
			{text}
		</Text>
	);
};
const SECOND = (text, style) => {
	return (
		<Text style={styles.TEXTSECOND} numberOfLines={1}>
			{text}
		</Text>
	);
};
const GOOGLE = (text, style) => {
	return (
		<>
			<Icon name="google" size={hp(4)} color={'#fff'} />
			<Text style={styles.TEXTGOOGLE} numberOfLines={1}>
				{text}
			</Text>
		</>
	);
};
const TEXT = (text, style) => {
	return <Text style={styles.TEXTBUTTON}>{text}</Text>;
};

const styles = StyleSheet.create({
	PRIMARY: {
		height: hp(9),
		width: wp(80),
		backgroundColor: color.primary,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 50,
		marginVertical: hp(1),
		alignSelf: 'center',
	},
	SECOND: {
		height: hp(9),
		width: wp(80),
		borderWidth: 1,
		borderColor: color.disable,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 50,
		marginVertical: hp(1),
		alignSelf: 'center',
	},
	GOOGLE: {
		alignSelf: 'center',
		height: hp(9),
		width: wp(80),
		borderColor: '#D72222',
		borderWidth: 1,
		alignItems: 'center',
		borderRadius: 50,
		marginVertical: hp(1),
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	TEXTPRIMARY: {
		color: color.dark,
		fontFamily: font.bold,
		fontSize: fontSize.large,
	},
	TEXTSECOND: {
		color: color.light,
		fontFamily: font.bold,
		fontSize: fontSize.large,
	},
	TEXTGOOGLE: {
		color: color.light,
		fontFamily: font.regular,
		fontSize: fontSize.large,
		right: wp(4),
	},
	TEXTBUTTON: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.primary,
	},
});
