import React from 'react';
import {AppStyle} from '../../../config/style';
import {Text,StyleSheet} from 'react-native';

const {fontSize, fontColor, font, color} = AppStyle;

const LOGO = ({text, sizeLogo}) => {
	return <Text style={sizeLogo == 'LARGE' ? styles.LARGE : styles.SMALL}>{text}</Text>;
};

export default LOGO;

const styles = StyleSheet.create({
	LARGE: {
		color: color.primary,
		fontSize:  fontSize.extraLarge,
		fontFamily: font.extraBoldItalic,
	},
	SMALL: {
		color: color.primary,
		fontSize:  fontSize.large,
		fontFamily: font.extraBoldItalic,
	},
});
