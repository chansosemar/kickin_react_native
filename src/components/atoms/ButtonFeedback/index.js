import React, {useState} from 'react';
import {AppStyle} from '../../../config/style';
import {
	Text,
	View,
	TouchableOpacity,
	StyleSheet,
	Modal,
	Linking,
} from 'react-native';
import * as G from '../../../config/style.js';
import {wp, fp, hp} from '../../../utils/Responsive';
import {useDispatch} from 'react-redux';

const {fontSize, fontColor, font, color} = AppStyle;

const ButtonFeedBack = ({
	text,
	onPress,
	status,
	style,
	item,
	navigation,
	checkDate,
}) => {
	const dispatch = useDispatch();

	return (
		<>
			<TouchableOpacity
				onPress={() => {
					checkDate == true && status == 'accepted'
						? navigation.navigate('Home')
						: checkDate == false && status != 'decline'
						? Linking.openURL(item.fieldTime.field.addressUrl)
						: (dispatch({
								type: 'GET_FIELD_DETAIL',
								payload: item.fieldTime.fieldId,
						  }),
						  navigation.navigate('Field Details'));
				}}>
				{checkDate == true && status == 'accepted'
					? FEEDBACK(text)
					: checkDate == false && status != 'decline'
					? DIRECTION(text)
					: BOOKAGAIN(text)}
			</TouchableOpacity>
		</>
	);
};

export default ButtonFeedBack;

const FEEDBACK = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Feedback
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Give us Feedback</Text>
			</View>
		</>
	);
};
const DIRECTION = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Direction
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Coming up Match</Text>
			</View>
		</>
	);
};
const BOOKAGAIN = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Book Again
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Please Book Again</Text>
			</View>
		</>
	);
};

const styles = StyleSheet.create({
	MODALCENTER: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 50,
	},
	MODALCARD: {
		width: wp(90),
		backgroundColor: color.dark,
		borderWidth: 2,
		borderColor: color.warning,
		borderRadius: 20,
		padding: 20,
		alignItems: 'center',
	},
	LABELFONT: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.light,
		textAlign: 'center',
		marginVertical: 10,
	},
	CENTER: {
		alignItems: 'center',
		alignSelf: 'center',
	},
});
