import React, {useState} from 'react';
import {AppStyle} from '../../../config/style';
import {Text, View, TouchableOpacity, StyleSheet, Modal} from 'react-native';
import * as G from '../../../config/style.js';
import {wp, fp, hp} from '../../../utils/Responsive';
import {useDispatch} from 'react-redux';

const {fontSize, fontColor, font, color} = AppStyle;

const ButtonStatus = ({text, onPress, status, style, item, navigation}) => {
	const dispatch = useDispatch();
	const [modalVisible, setModalVisible] = useState(false);

	return (
		<>
			<TouchableOpacity
				style={[
					status == 'accepted'
						? G.STSBTNACPT
						: status == 'decline'
						? G.STSBTNDCN
						: G.STSBTNWFP,
				]}
				onPress={() => {
					status == 'accepted'
						? (dispatch({type: 'GET_TICKET', payload: item.id}),
						  navigation.navigate('Ticket', {item}))
						: status == 'waiting for payment'
						? (dispatch({type: 'GET_TRX', payload: item.id}),
						  navigation.navigate('Upload Receipt', item.transactions[0].id))
						: setModalVisible(true);
				}}>
				{status == 'accepted'
					? ACCEPETED(text)
					: status == 'decline'
					? DECLINE(text)
					: OTHER(text)}
			</TouchableOpacity>
			<Modal animationType="slide" transparent={true} visible={modalVisible}>
				<View style={styles.MODALCENTER}>
					<View style={styles.MODALCARD}>
						<Text style={G.STATUSFONTDCN}>attention</Text>
						{status == 'decline' ? (
							<Text style={styles.LABELFONT}>
								Your booking has been{' '}
								<Text style={{color: color.warning}}>DECLINED</Text> due we did
								not receive your transaction proof within 24 hours , Please book
								again.
							</Text>
						) : (
							<Text style={styles.LABELFONT}>
								We are currently processing your payment, Please wait for a
								while.
							</Text>
						)}

						<TouchableOpacity onPress={() => setModalVisible(false)}>
							<Text style={G.STATUSFONTDCN}>Close</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
		</>
	);
};

export default ButtonStatus;

const ACCEPETED = (text, style) => {
	return (
		<Text style={G.STATUSFONT} numberOfLines={1}>
			{text}
		</Text>
	);
};
const DECLINE = (text, style) => {
	return (
		<Text style={G.STATUSFONTDCN} numberOfLines={1}>
			{text}
		</Text>
	);
};
const OTHER = (text, style) => {
	return (
		<Text style={G.STATUSFONT} numberOfLines={1}>
			{text}
		</Text>
	);
};

const styles = StyleSheet.create({
	MODALCENTER: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 50,
	},
	MODALCARD: {
		width: wp(90),
		backgroundColor: color.dark,
		borderWidth: 2,
		borderColor: color.warning,
		borderRadius: 20,
		padding: 20,
		alignItems: 'center',
	},
	LABELFONT: {
		fontFamily: font.light,
		fontSize: fontSize.medium,
		color: color.light,
		textAlign: 'center',
		marginVertical: 10,
	},
});
