import React, {useState} from 'react';
import {AppStyle} from '../../../config/style';
import {
	Text,
	View,
	TouchableOpacity,
	StyleSheet,
	Modal,
	Linking,
	Image,
} from 'react-native';
import * as G from '../../../config/style.js';
import {wp, fp, hp} from '../../../utils/Responsive';
import {useDispatch} from 'react-redux';
import moment from 'moment';
import {Rupiah} from '../../../common/function/allFunction';

const {fontSize, fontColor, font, color} = AppStyle;

const PaymentAdmin = ({
	text,
	onPress,
	status,
	style,
	items,
	navigation,
	checkDate,
}) => {
	const dispatch = useDispatch();
	const accept = () => {
		const data = {
			id: items.id,
			isPaid: true,
			isAccepted: true,
			status: 'paid',
		};
		dispatch({type: 'PATCH_PAYMENT', payload: data});
	};
	const decline = () => {
		const data = {
			id: items.id,
			isPaid: false,
			isAccepted: false,
			status: 'unpaid',
		};
		dispatch({type: 'PATCH_PAYMENT', payload: data});
	};
	return (
		<View>
			<View style={styles.CONTREVIEW}>
				<View style={G.CARDREVIEW}>
					<View style={styles.HEADER}>
						<Text style={G.BOOKTITLE}>
							{items.transaction.booking.fieldTime.field.name}{' '}
						</Text>
						<Text style={G.BOOKTITLE}>
							{moment(items.transaction.booking.startTime).format('Do MMM YY')}{' '}
						</Text>
					</View>

					<Image source={{uri: items.receiptPhoto}} style={styles.IMG} />
					<View style={styles.PRICE}>
						<Text style={G.COMINGSOON}>
							Rp {Rupiah(items.transaction.totalPrice)}
						</Text>
					</View>

					<View style={styles.HEADER}>
						<TouchableOpacity 
							onPress={() => accept()}
							style={styles.BUTTONPRIMARY}>
							<Text style={G.BOOKTITLE}>Accept</Text>
						</TouchableOpacity>
						<TouchableOpacity 
							onPress={() => decline()}
							style={styles.BUTTONRED}>
							<Text style={G.BOOKTITLE}>Decline</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</View>
	);
};

export default PaymentAdmin;

const FEEDBACK = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Feedback
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Give us Feedback</Text>
			</View>
		</>
	);
};
const DIRECTION = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Direction
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Coming up Match</Text>
			</View>
		</>
	);
};
const BOOKAGAIN = (text, style) => {
	return (
		<>
			<View style={G.DIRECTIONBTN}>
				<Text style={G.BOOKTITLE} numberOfLines={1}>
					Book Again
				</Text>
			</View>
			<View style={styles.CENTER}>
				<Text style={G.COMINGSOON}>Please Book Again</Text>
			</View>
		</>
	);
};

const styles = StyleSheet.create({
	BUTTONPRIMARY: {
		width: wp(30),
		backgroundColor: color.primary,
		padding: hp(2),
		borderRadius: 50,
		marginVertical: hp(1),
		alignItems: 'center',
	},
	BUTTONRED: {
		width: wp(30),
		backgroundColor: color.warning,
		padding: hp(2),
		borderRadius: 50,
		marginVertical: hp(1),
		alignItems: 'center',
	},
	MARGINHIGH: {
		marginVertical: hp(3),
	},
	HEADER: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	CONTREVIEW: {
		width: wp(90),
		alignItems: 'center',
	},
	IMG: {
		width: wp(80),
		height: hp(15),
		borderRadius: 20,
		alignSelf: 'center',
		marginVertical: hp(1),
	},
	PRICE: {
		alignSelf: 'center',
		marginVertical: hp(1),
	},
});
