import React, {useState} from 'react';
import {AppStyle} from '../../../config/style';
import {
	Text,
	View,
	TouchableOpacity,
	StyleSheet,
	Modal,
	TextInput,
} from 'react-native';
import {useDispatch} from 'react-redux'
import * as G from '../../../constant/style';
import {wp, fp, hp} from '../../../utils/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Rating} from 'react-native-elements';

const {fontSize, fontColor, font, color} = AppStyle;

const Review = ({text, onPress, typeButton, style, items}) => {
	const [modalVisible, setModalVisible] = useState(false);
	const [resRating, setResRating] = useState();
	const [comment, setComment] = useState();
	const dispatch = useDispatch()

	const submit = () => {
		const data = {
			id: items.id,
			rating: resRating,
			comment: comment,
		};
		dispatch({type: 'EDIT_REVIEW', payload: data});
		setModalVisible(false);
	};

	return (
		<View style={styles.CONTREVIEW} key={items.id}>
			<View style={G.CARDREVIEW}>
				<Text style={G.TITLEREVIEW}>{items.field.name}</Text>
				<View style={styles.COMPONENT}>
					<View style={styles.RATING}>
						<Rating
							imageSize={20}
							readonly
							startingValue={items.rating}
							tintColor={'#222'}
						/>
					</View>
					<View style={styles.ICON}>
						<TouchableOpacity onPress={() => setModalVisible(true)}>
							<Icon
								name="pencil"
								size={20}
								color="#53C9C2"
								style={{marginHorizontal: 10}}
							/>
						</TouchableOpacity>
					</View>
				</View>
				<Text style={G.TXTREVIEW}>{items.comment}</Text>
			</View>
			<Modal animationType="slide" transparent={true} visible={modalVisible}>
				<View style={styles.MODALCENTER}>
					<View style={G.MODALCARD}>
						<View style={styles.ICON}>
							<Text style={G.TITLEREVIEW}>{items.field.name}</Text>
							<TouchableOpacity onPress={() => setModalVisible(false)}>
								<Icon
									name="close"
									size={20}
									color="#D72222"
									style={{marginHorizontal: 10}}
								/>
							</TouchableOpacity>
						</View>
						<View style={styles.COMPONENT}>
							<View style={styles.RATING}>
								<Rating
									imageSize={20}
									count={5}
									defaultRating={items.rating}
									onFinishRating={(rating) => setResRating(rating)}
									tintColor={'#222'}
								/>
							</View>
						</View>
						<TextInput
							multiline={true}
							value={comment}
							onChangeText={(text) => setComment(text)}
							style={G.FORMINPUT}
						/>
						<TouchableOpacity onPress={() => submit()} style={G.MAINBTN}>
							<Text style={G.TXTMAINBTN}>Submit</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
		</View>
	);
};

export default Review;

const styles = StyleSheet.create({
	CONTREVIEW: {
		width: '90%',
	},
	RATING: {
		flexDirection: 'row',
	},
	COMPONENT: {
		flexDirection: 'row',
		marginVertical: 10,
		justifyContent: 'space-between',
	},
	ICON: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	MODALCENTER: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 50,
	},
});
