import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Image,
} from 'react-native';
import * as G from '../constant/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import {connect, useDispatch, useSelector} from 'react-redux';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  convertDateToString,
  convertTimeToString,
  Rupiah,
} from '../common/function/allFunction';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default function UploadReceiptPage(props) {
  const dataId = props.route.params;
  const transaction = useSelector((state) => state.transaction.data.data);
  const fieldData = useSelector((state) => state.fieldDetail.data);
  const dispatch = useDispatch();
  const baseUrl = 'http://kickin.southeastasia.cloudapp.azure.com';
  const [image, setImage] = useState();
  const [rawImage, setRawImage] = useState({});
  const [modalVisible, setModalVisible] = useState(false);
  const [rentPrice, setRentPrice] = useState();
  const [rentPrice1, setRentPrice1] = useState();
  const [rentPrice2, setRentPrice2] = useState();
  const [rentPrice3, setRentPrice3] = useState();
  const [fieldPrice, setFieldPrice] = useState();

  console.log('ini kiriman >>>', dataId);

  useEffect(() => {
    if (transaction != null) {
      const temp = [];
      transaction.getRentData.map((item, index) => {
        temp.push(item.totalPrice);
        setRentPrice(temp.reduce((a, b) => a + b, 0));
      });
    }
    if (transaction != null) {
      setFieldPrice(transaction.getFieldData.fieldPrice);
    }
  }, [transaction]);

  function pickImage() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
          data: response.data,
        };

        setRawImage(source);
        setImage(response.uri);
      }
    });
  }

  const submit = () => {
    const data = new FormData();
    data.append('file', rawImage);
    const dataPayment = {
      data,
      id: dataId,
    };
    dispatch({type: 'POST_PAYMENT', payload: dataPayment});
    dispatch({type: 'GET_BOOKING_USER'});
    console.log(dataPayment);
  };

  return (
    <View style={G.CONTAINER}>
      <View style={G.CONTENTCHECK}>
        <View style={{marginVertical: 5}}>
          <Text style={[G.HEADERFONTCHECK, styles.MARGIN]}>Transfer</Text>
          <View style={styles.LINE}></View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONT, styles.FLEX]}>Name</Text>
            <Text style={[G.LABELFONT, styles.FLEX]}>PT. KICKIN MAJU JAYA</Text>
          </View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONT, styles.FLEX]}>No. Account</Text>
            <Text style={[G.LABELFONT, styles.FLEX]}>525533671</Text>
          </View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONTCHECK, styles.FLEX]}>Total</Text>
            <Text style={[G.LABELFONTCHECK, styles.FLEX]}>
              Rp {Rupiah(fieldPrice + rentPrice)}
            </Text>
          </View>
        </View>
        <View style={styles.LINE}></View>
        <View style={{marginVertical: 5}}>
          <Text style={[G.LABELFONTCHECK, styles.MARGIN]}>UPLOAD RECEIPT</Text>
          <View style={styles.MARGIN}></View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#53C9C2',
                borderWidth: 1,
                padding: 10,
                borderRadius: 10,
              }}>
              {image != null ? (
                <Image
                  source={{uri: image}}
                  style={{
                    width: 150,
                    height: 200,
                    alignItems: 'center',
                    borderRadius: 10,
                    padding: 10,
                  }}
                />
              ) : (
                <TouchableOpacity
                  onPress={() => pickImage()}
                  style={{
                    width: 150,
                    height: 200,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#313131',
                    padding: 10,
                    borderRadius: 10,
                  }}>
                  <Text>
                    {' '}
                    <Icon name="camera" size={20} color={'#53C9C2'} />{' '}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.FLEX}>
              <Text style={[G.SPCTXT, styles.CENTER]}>
                After upload your receipt, wait for max 24 Hours. Please Check
                your History Tab Frequently
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.MARGIN}></View>
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={G.MAINBTN}>
          <Text style={G.TXTMAINBTN}>Submit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            dispatch({type: 'GET_BOOKING_USER'});
            props.navigation.navigate('History');
          }}
          style={G.GOOGLEBTN}>
          <Text style={G.TXTLATERBTN}>Later</Text>
        </TouchableOpacity>
      </View>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.MODALCENTER}>
          <View style={G.MODALASK}>
            <Text style={G.TITLEREVIEW}>Are you sure ?</Text>
            <View style={styles.BTNPOS}>
              <TouchableOpacity
                style={G.YESBTN}
                onPress={() => {
                  submit();
                  setModalVisible(false);
                  props.navigation.navigate('Home');
                }}>
                <Text style={G.TXTYESBTN}>Yes</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={G.NOBTN}
                onPress={() => setModalVisible(false)}>
                <Text style={G.TXTNOBTN}>No</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  FLEX: {
    flex: 1,
  },
  MARGIN: {
    marginVertical: 5,
  },
  LINE: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#fff',
    marginVertical: 10,
  },
  CENTER: {
    textAlign: 'center',
    padding: 5,
  },
  MODALCENTER: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  BTNPOS: {
    position: 'absolute',
    bottom: 20,
  },
});

//
// {"getFieldData":
//  {"endTime": "2020-12-21T18:49:03.000Z", "fieldPrice": 150, "fieldTime":
//   {"field": [Object], "id": "ee530b9a-0d87-4dc4-a8bd-054f982cde94"},
//    "startTime": "2020-12-21T18:48:59.000Z"},
//     "getRentData": [{"quantity": "1", "rentItem": [Object], "size": "45", "totalPrice": 15000},
//     {"quantity": "1", "rentItem": [Object], "size": "l", "totalPrice": 10000},
//     {"quantity": "1", "rentItem": [Object], "size": "l", "totalPrice": 5000}]}
