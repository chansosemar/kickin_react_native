import React, {useState} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ScrollView,
  Modal,
  ImageBackground,
} from 'react-native';
import * as G from '../constant/style.js';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
// SCREENS
import Header from './components/HeaderComponent';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default function EditProfilePage(props) {
  // REDUX
  const profile = useSelector((state) => state.profile.data[0]);
  const dispatch = useDispatch();
  // HOOKS
  const [fullname, setFullname] = useState(profile.fullname);
  const [email, setEmail] = useState(profile.email);
  const [phone, setPhone] = useState(profile.phoneNumber);
  const [bio, setBio] = useState(profile.bio);
  const [image, setImage] = useState(profile.photo);
  const [rawImage, setRawImage] = useState({});
  const navigation = useNavigation();
  const baseUrl = 'http://kickin.southeastasia.cloudapp.azure.com';
  const [modalVisible, setModalVisible] = useState(false);

  // SUBMIT EDIT PROFILE
  const submit = () => {
    const dataEditProfile = {
      fullname: fullname,
      email: email,
      bio: bio,
      photo: image,
      phoneNumber: phone,
    };
    dispatch({type: 'EDIT_PROFILE', payload: dataEditProfile});
    navigation.navigate('Profile');
  };

  const cancel = () => {
    navigation.navigate('Profile');
  };

  // IMAGE PICKER
  function pickImage() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
          data: response.data,
        };

        setRawImage(source);
        setImage(response.uri);
      }
    });
  }

  async function uploadImage() {
    const token = await AsyncStorage.getItem('APP_AUTH_TOKEN');
    const data = new FormData();
    data.append('file', rawImage);

    axios({
      method: 'POST',
      url: baseUrl + '/files',
      data: data,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('berhasil upload');
        setImage(res.data);
        setModalVisible(true);
      })
      .catch((err) => {
        setModalVisible(true);
        console.log('error', err);
      });
  }

  return (
    <View style={{backgroundColor: '#222'}}>
      {profile.photo == null ? (
        <ImageBackground
          source={require('../../assets/user.png')}
          style={G.PROFILEIMG}>
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 0, y: 0}}
            colors={['#222', 'transparent']}
            style={G.PROFILEGRADIENT}>
            <Header />
          </LinearGradient>
        </ImageBackground>
      ) : (
        <ImageBackground source={{uri: image}} style={G.PROFILEIMG}>
          <LinearGradient
            start={{x: 0, y: 1}}
            end={{x: 0, y: 0}}
            colors={['#222', 'transparent']}
            style={G.PROFILEGRADIENT}>
            <Header />
          </LinearGradient>
        </ImageBackground>
      )}
      <View style={G.CONTAINER}>
        <View style={G.CONTENTPROFILE}>
          <View style={{width: G.DIMENSWIDTH * 0.9}}>
            <TouchableOpacity onPress={() => pickImage()}>
              <Text style={G.EDITPICTUREBTN}>Edit Profile Picture</Text>
            </TouchableOpacity>
          </View>
          <View>
            <ScrollView>
              <Text style={G.LABELFONTPROFILE}>Name</Text>
              <TextInput
                value={fullname}
                onChangeText={(text) => setFullname(text)}
                style={G.FORMINPUT}
              />

              <Text style={G.LABELFONTPROFILE}>Email</Text>
              <TextInput
                value={email}
                onChangeText={(text) => setFullname(text)}
                style={G.FORMINPUT}
              />

              <Text style={G.LABELFONTPROFILE}>Phone</Text>
              <TextInput
                value={phone}
                onChangeText={(text) => setPhone(text)}
                style={G.FORMINPUT}
              />

              <Text style={G.LABELFONTPROFILE}>Bio</Text>
              <TextInput
                multiline={true}
                value={bio}
                onChangeText={(text) => setBio(text)}
                style={G.FORMINPUT}
              />
              <View style={styles.BTNWRAPPER}>
                <TouchableOpacity style={G.LGTBTN} onPress={() => cancel()}>
                  <Text style={G.TXTLGTBTN}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={G.EDITBTN}
                  onPress={() => uploadImage()}>
                  <Text style={G.TXTEDITBTN}>Submit</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
          <View style={styles.MODALCENTER}>
            <View style={G.MODALASK}>
              <Text style={G.TITLEREVIEW}>Are you sure ?</Text>
              <View style={styles.BTNPOS}>
                <TouchableOpacity
                  style={G.YESBTN}
                  onPress={() => {
                    submit();
                    setModalVisible(false);
                  }}>
                  <Text style={G.TXTYESBTN}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={G.NOBTN}
                  onPress={() => setModalVisible(false)}>
                  <Text style={G.TXTNOBTN}>No</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  BTNWRAPPER: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: 10,
  },
  MODALCENTER: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  BTNPOS: {
    position: 'absolute',
    bottom: 20,
  },
});
