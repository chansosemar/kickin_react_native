import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator,
  Modal,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import * as GLOBAL from '../constant';
import * as G from '../constant/style.js';
import DateTimePicker from '@react-native-community/datetimepicker';
import {connect} from 'react-redux';
import {addBookingAction} from '../redux/action/bookingAction';
import {getRentalListAction} from '../redux/action/rentalKitAction';
import {
  convertDateToString,
  convertTimeToString,
  Rupiah,
} from '../common/function/allFunction';
import Header from './components/HeaderComponent';
import DropDownPicker from 'react-native-dropdown-picker';
import SplashScreen from './SplashScreen';
import moment from 'moment';

function BookPage({
  navigation,
  route,
  addBooking,
  isLoadingAdd
}) {
  const {item} = route.params;
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [duration, setDuration] = useState(1);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectDuration, setSelectDuration] = useState();
  const [qtyShocks, setQtyShocks] = useState('0');
  const [qtyShoes, setQtyShoes] = useState('0');
  const [sizeShoes, setSizeShoes] = useState('0');
  const [qtyUniform, setQtyUniform] = useState('0');
  const [sizeUniform, setSizeUniform] = useState('0');
  const [listBooking, setListBooking] = useState();

  const submit = () => {
    const data = {
      id: item.id,
      startTime: startTime,
      endTime: endTime
    }
    addBooking(data);
    if(isLoadingAdd){
      <SplashScreen/>
    } else {
      navigation.navigate('Rental Kit');
    }
   
  };

  // LOCAL TIME
  const offsetDate = date.getTimezoneOffset() * 60 * 1000;
  const localDate = new Date(date.getTime() - offsetDate);

  // START TIME
  const offsetStartTime = time.getTimezoneOffset() * 60 * 1000;
  const startTime = new Date(time.getTime() - offsetStartTime);
  console.log('start', startTime);

  // END TIME
  const convertDuration = duration / 1;
  const endTime = new Date(
    time.getTime() + 25200000 + convertDuration * 3600000,
  );
  console.log('endtime', endTime);

  const onChange = (event, selectedValue) => {
    setShow(Platform.OS === 'ios');
    if (mode == 'date') {
      const currentDate = selectedValue || new Date();
      setDate(currentDate);
      setMode('time');
      setShow(Platform.OS !== 'ios');
    } else {
      const selectedTime = selectedValue || new Date();
      setTime(selectedTime);
      setShow(Platform.OS === 'ios');
      setMode('date');
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  

  return (
    <View style={G.CONTAINER}>
      <ImageBackground
        source={{uri: item.fieldPhotos[0].photoUrl}}
        style={G.PROFILEIMG}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 0, y: 0}}
          colors={['#222', 'transparent']}
          style={G.PROFILEGRADIENT}>
          <Header />
          <View style={styles.CONTENT}>
            <Text style={G.FIELDNAME}>{item.name}</Text>
            <Text style={G.FIELDNAME}>Rp {Rupiah(item.price * duration)}</Text>
          </View>
        </LinearGradient>
      </ImageBackground>
      <View style={{alignSelf: 'center'}}>
        <View style={styles.MARGIN}>
          <Text style={G.LABELFONTBOOK}>Schedule</Text>
          <View style={styles.dateTimeStyle}>
            <Text style={G.LABELFONT}>Date </Text>
            <Text style={G.LABELFONT}>
              {date ? convertDateToString(date) : 'Please =>'}
            </Text>
            <Text style={G.LABELFONT}>Time </Text>
            <Text style={G.LABELFONT}>
              {time ? convertTimeToString(time) : 'Please =>'}
            </Text>
          </View>
          <View style={styles.dateTimeStyle}>
          </View>
          <TouchableOpacity onPress={showDatepicker} style={GLOBAL.PICKBUTTON}>
            <Text style={GLOBAL.TXTPICKBUTTON}>Pick Schedule</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.MARGIN}>
          <Text style={G.LABELFONTBOOK}>Duration</Text>
          <View style={styles.dateTimeStyle}>
            <TouchableOpacity
              style={
                selectDuration == 1
                  ? GLOBAL.PICKDURATIONBTN
                  : GLOBAL.DURATIONBTN
              }
              onPress={() => {
                setSelectDuration(1);
                setDuration(1);
              }}>
              <Text
                style={
                  selectDuration == 1
                    ? GLOBAL.SELECTDURATIONFONT
                    : GLOBAL.SELECTFONT
                }>
                1
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                selectDuration == 2
                  ? GLOBAL.PICKDURATIONBTN
                  : GLOBAL.DURATIONBTN
              }
              onPress={() => {
                setSelectDuration(2);
                setDuration(2);
              }}>
              <Text
                style={
                  selectDuration == 2
                    ? GLOBAL.SELECTDURATIONFONT
                    : GLOBAL.SELECTFONT
                }>
                2
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                selectDuration == 3
                  ? GLOBAL.PICKDURATIONBTN
                  : GLOBAL.DURATIONBTN
              }
              onPress={() => {
                setSelectDuration(3);
                setDuration(3);
              }}>
              <Text
                style={
                  selectDuration == 3
                    ? GLOBAL.SELECTDURATIONFONT
                    : GLOBAL.SELECTFONT
                }>
                3
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                selectDuration == 4
                  ? GLOBAL.PICKDURATIONBTN
                  : GLOBAL.DURATIONBTN
              }
              onPress={() => {
                setSelectDuration(4);
                setDuration(4);
              }}>
              <Text
                style={
                  selectDuration == 4
                    ? GLOBAL.SELECTDURATIONFONT
                    : GLOBAL.SELECTFONT
                }>
                4
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => submit()}
            >
            <View style={GLOBAL.MAINBUTTON}>
              <Text style={GLOBAL.TXTMAINBUTTON}>Next</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View>
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              timeZoneOffsetInMinutes={0}
              value={date}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onChange}
            />
          )}
        </View>
      </View>
    </View>
  );
}

const mapStateToProps = (state) => ({
  isLoadingAdd : state.booking.isLoading
});

const mapDispatchToProps = (dispatch) => ({
  addBooking: (data) => dispatch(addBookingAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BookPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#222',
    height: GLOBAL.DIMENSHEIGHT,
  },
  CONTENT: {
    marginBottom: 50,
  },
  MARGIN: {
    marginVertical: 5,
    width: GLOBAL.DIMENSWIDTH * 0.9,
    paddingBottom: 10,
  },
  bookButton: {
    position: 'absolute',
    top: GLOBAL.DIMENSHEIGHT * 0.88,
  },
  dateTimeStyle: {
    marginVertical: 5,
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: GLOBAL.DIMENSWIDTH * 0.9,
    height: '45%',
    margin: 10,
    backgroundColor: '#313131',
    borderRadius: 5,
    padding: 10,
    alignItems: 'center',
  },
  marginModal: {
    width: '100%',
    paddingHorizontal: 10,
    marginVertical: 8,
  },
  marginText: {
    marginVertical: 8,
  },
  container: {
    position: 'absolute',
    width: '100%',
    height: 200,
  },
  redText: {
    color: '#D72222',
  },
  rentalModal: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginTop: 10,
  },
});

