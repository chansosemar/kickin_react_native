import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import * as G from '../../config/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';
import {GoogleSignin} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import {wp, fp, hp} from '../../utils/Responsive';
// ATOMS
import Button from '../../components/atoms/Button';
import Logo from '../../components/atoms/Logo';
// SCREENS
import SplashScreen from '../SplashScreen';

export default function LoginPage({navigation}) {
  // REDUX
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.isLoading);
  // HOOKS
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [message, setMessage] = useState(null);
  const [hide, setHide] = useState(true);
  const [googleToken, setGoogleToken] = useState();

  //Google Sign in
  GoogleSignin.configure({
    webClientId: '257695633255-8ut398jnnq4f4s5pj99irv46mishs6i2',
  });

  async function onGoogleButtonPress() {
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    setGoogleToken(googleCredential.token);
    const data = {
      token: googleCredential.token
    };
    dispatch({type: 'LOGIN_GOOGLE', payload: data});
    return auth().signInWithCredential(googleCredential);
  }
  console.log('inigoogle>>', googleToken);
  
  // Show and Hide Password
  const show = () => {
    setHide(false);
    setTimeout(() => {
      setHide(true);
    }, 1000);
  };

  // Submit Email
  const submit = () => {
    let mailformat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    let passwordformat = /^(?=.*[0-9])(?=.*[A-Z]).{8,32}$/g;

    if (!email) {
      setMessage('Email Must be Field !!');
    } else if (!email.match(mailformat)) {
      setMessage('Email Invalid !!');
    } else if (!password) {
      setMessage('Password Must be Field !!');
    } else {
      const data = {
        email,
        password,
      };
      dispatch({type: 'LOGIN', payload: data});
    }
  };

  // Keyboard Setting
  const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0;

  return (
    <View style={G.CONTAINER}>
      {isLoading ? (
        <SplashScreen />
      ) : (
        <View style={G.CONTENT}>
          <View style={styles.MARGINLOW}>
            <Logo
            text='KICKIN'
            sizeLogo='SMALL'
            />
          </View>
          <KeyboardAvoidingView
            behavior="position"
            keyboardVerticalOffset={keyboardVerticalOffset}
            style={G.CONTENT}>
            <View style={styles.MARGINLOW}>
              <Text style={G.WARNTEXT}>{message}</Text>
              <Text style={G.LABELFONT}>Email</Text>
              <TextInput
                value={email}
                onChangeText={(text) => setEmail(text)}
                style={G.FORMINPUT}
              />
              <Text style={G.LABELFONT}>Password</Text>
              <View>
                <TextInput
                  value={password}
                  onChangeText={(text) => setPassword(text)}
                  style={G.FORMINPUT}
                  secureTextEntry={hide}
                />
                <TouchableOpacity onPress={() => show()} style={G.SHOWPASSWORD}>
                  <Icon name="eye" size={hp(3)} color={'#fff'} />
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
          <Button text="Log in" typeButton="PRIMARY" onPress={() => submit()} />
          <Button
            text="Log in with Google"
            typeButton="GOOGLE"
            onPress={() => onGoogleButtonPress()}
          />
          <View style={styles.MARGINHIGH}>
            <Button
            text="Don't have an account? Sign Up"
            typeButton="TEXT"
            onPress={() => navigation.navigate('Register')}
          />
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  MARGINLOW: {
    marginVertical: hp(4),
  },
  MARGINHIGH: {
    marginVertical: hp(6),
    alignSelf: 'center',
  },
});
