import React, {useState} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
} from 'react-native';

import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  convertDateToString,
  convertTimeToString,
  checkDate,
} from '../../common/function/allFunction';
import * as G from '../../config/style.js';
import {wp, fp, hp} from '../../utils/Responsive';
import moment from 'moment';
import SplashScreen from '../SplashScreen';
import NotLoggedIn from '../../components/molecules/NotLoggedIn';
import NotBookingYet from '../../components/molecules/NotBookingYet';
import StatusBooking from '../../components/molecules/StatusBooking';

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function HistoryPage(props) {
  // REDUX
  const statusLogin = useSelector((state) => state.auth.isLoggedIn);
  const userBookingData = useSelector(
    (state) => state.getUserBooking.data.getUserBooking,
  );
  const isLoading = useSelector((state) => state.getUserBooking.isLoading);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // HOOKS
  const [progress, setProgress] = useState('waiting');
  const A = 'waiting';
  const B = 'decline';
  const C = 'accepted';

  return (
    <View style={styles.BOOKCONTAINER}>
      <View style={G.CONTAINER}>
        <Text style={G.HISTORYTITLE}>Booking Status</Text>
        <View style={styles.FLEX}>
          <Text
            style={[progress == A ? G.HISTORYTITLEACTIVE : G.HISTORYTITLE]}
            onPress={() => setProgress(A)}>
            Booking
          </Text>
          <Text
            style={[progress == B ? G.HISTORYTITLEACTIVE : G.HISTORYTITLE]}
            onPress={() => setProgress(B)}>
            Decline
          </Text>
          <Text
            style={[progress == C ? G.HISTORYTITLEACTIVE : G.HISTORYTITLE]}
            onPress={() => setProgress(C)}>
            Ticket
          </Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{marginBottom: hp(13), width: '100%'}}>
          <>
            {!userBookingData && statusLogin ? (
              <NotBookingYet />
            ) : isLoading ? (
              <SplashScreen />
            ) : statusLogin ? (
              <View>
                {userBookingData.map((item, index) =>
                  item.status.includes(progress) ? (
                    <StatusBooking
                     item={item} 
                     navigation={navigation} 
                     key={item.id} />
                  ) : null,
                )}
              </View>
            ) : (
              <NotLoggedIn 
                navigation={navigation}
                />
            )}
          </>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  BOOKCONTAINER: {
    padding: wp(5),
    alignSelf: 'center',
    width: wp(100),
    backgroundColor: color.dark,
  },
  FLEX: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});
