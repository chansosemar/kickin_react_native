import React, {useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import * as G from '../../config/style.js';
import SplashScreen from 'react-native-splash-screen';
import {useDispatch, useSelector} from 'react-redux';
import {LandingCover} from '../../config/images';
import {wp, fp, hp} from '../../utils/Responsive';
// ATOMS
import Button from '../../components/atoms/Button';
import Logo from '../../components/atoms/Logo';
import Splash from '../SplashScreen';


export default function LandingPage({navigation}) {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.isLoading);

  useEffect(() => {
    SplashScreen.hide();
  });

  const skip = () => {
    dispatch({type: 'GET_FIELD'});
    navigation.navigate('Home');
  };

  const submit = () => {
    const data = {
      email: 'admin@kickin.com',
      password: 'admin123',
    };
    dispatch({type: 'LOGIN_ADMIN', payload: data});
  };

  return (
    <View style={G.CONTAINER}>
      {isLoading ? (
        <Splash />
      ) : (
        <ImageBackground source={LandingCover} style={styles.IMGBACKGROUND}>
          <View style={styles.CONTAINER}>
            <View style={styles.SETLOGO}>
              <TouchableOpacity onPress={() => submit()}>
                <Logo sizeLogo="LARGE" text="KICKIN" />
              </TouchableOpacity>
            </View>
            <Button
              onPress={() => navigation.navigate('Login')}
              text="Log in"
              typeButton="PRIMARY"
            />
            <Button onPress={() => skip()} text="Skip" typeButton="SECOND" />
            <View style={styles.SETSPCTXT}>
              <Text style={G.SPCTXT}>
                By continuing you agree Kickin's Terms of Services and Privacy
                Policy{' '}
              </Text>
            </View>
          </View>
        </ImageBackground>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  IMGBACKGROUND: {
    width: wp(100),
    flex: 1,
  },
  CONTAINER: {
    alignSelf: 'center',
    marginTop: hp(40),
    width: wp(80),
  },
  SETLOGO: {
    marginVertical: hp(5),
    alignSelf: 'center',
  },
  SETSPCTXT: {
    marginVertical: hp(5),
  },
});
