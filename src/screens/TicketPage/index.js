import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import {useSelector} from 'react-redux';
import * as G from '../../config/style.js';
import {
  convertDateToString,
  convertTimeToString,
} from '../../common/function/allFunction';
import SplashScreen from '../SplashScreen';
import {wp, fp, hp} from '../../utils/Responsive';

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function TicketPage() {
  const ticket = useSelector((state) => state.getTicket.data[0]);
  const isLoading = useSelector((state) => state.getTicket.isLoading);

  console.log('tiket', ticket);

  // COUNT DURATION & PRICE
  const Duration = (a, b) => {
    const temp = [];
    const convertStart = convertTimeToString(a);
    const convertEnd = convertTimeToString(b);
    const awal = convertStart.split(':');
    const akhir = convertEnd.split(':');
    temp.push(awal[0]);
    temp.push(akhir[0]);
    return temp[1] - temp[0];
  };

  return (
    <View style={G.CONTAINER}>
      {isLoading ? (
        <SplashScreen />
      ) : (
        <View style={G.CONTENTTICKET}>
          <View style={styles.MARGIN}>
            <Text style={[G.HEADERFONTCHECK, styles.MARGIN]}>Ticket</Text>
            <View style={styles.LINE}></View>
            <Text style={G.BOOKCODE} numberOfLines={3}>
              {ticket.id}
            </Text>
            <ScrollView showsVerticalScrollIndicator={false} style={{marginTop: hp(1)}}>
              <View style={styles.LINE}></View>
              <Text style={G.LABELTICKETFONT}>Field Name</Text>
              <Text style={G.FILLTICKETFONT}>
                {ticket.fieldTime.field.name}
              </Text>
              <Text style={G.LABELTICKETFONT}>Field Address</Text>
              <Text style={G.FILLTICKETFONT}>
                {ticket.fieldTime.field.address}
              </Text>
              <Text style={G.LABELTICKETFONT}>Date & Time</Text>
              <Text style={G.FILLTICKETFONT}>
                {convertDateToString(ticket.startTime)} //{' '}
                {convertTimeToString(ticket.endTime)}
              </Text>
              <Text style={G.LABELTICKETFONT}>Duration</Text>
              <Text style={G.FILLTICKETFONT}>
                {Duration(ticket.startTime, ticket.endTime)} Hour(s)
              </Text>
              <View style={styles.MARGIN2}>
                <Text style={G.HOWFONT}>
                  1. Visit the futsal field that you choose
                </Text>
                <Text style={G.HOWFONT}>
                  2. Show this ticket to the counter at least 5 minute before
                  the game
                </Text>
                <Text style={G.HOWFONT}>
                  3. If do you want to share this ticket to your friends, you
                  only need to screenshot this ticket
                </Text>
                <Text style={G.HOWFONT}>
                  4. Our counter will check your booking code so make sure keep
                  the booking code protected.
                </Text>
                <Text style={G.HOWFONT}>5. You're ready to Kick</Text>
              </View>
              <View style={styles.LINE}></View>
            </ScrollView>
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  FLEX: {
    flex: 1,
  },
  CENTER: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  MARGIN: {
    marginVertical: 5,
  },
  MARGIN2: {
    marginVertical: hp(2.5),
  },
  LINE: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#fff',
    marginVertical: hp(1.5),
  },
});
