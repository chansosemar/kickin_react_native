import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Image,
  ImageBackground,
} from 'react-native';
import * as GLOBAL from '../../constant';
import * as G from '../../config/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import {useDispatch} from 'react-redux';
import {wp, fp, hp} from '../../utils/Responsive';
import {Rupiah} from '../../common/function/allFunction';
// SCREENS
import {HeaderBack} from '../components/HeaderComponent';

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function SearchPage(props) {
  const dispatch = useDispatch()
  const navigation = useNavigation();
  const [data, setData] = useState();
  const [find, setFind] = useState();
  const keyExtractor = (item) => item.id;

  function search() {
    const baseUrl = 'http://kickin.southeastasia.cloudapp.azure.com';

    axios({
      method: 'GET',
      url: baseUrl + '/fields/location?q=' + find,
    })
      .then((res) => {
        console.log('res', res.data);
        const hasilData = res.data;
        let temp = [];
        for (const item of hasilData) {
          temp = temp.concat(item.fields);
        }
        setData(temp);
      })
      .catch((err) => {
        console.log('error', err);
      });
  }

  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        dispatch({type: 'GET_FIELD_DETAIL', payload: item.id});
        navigation.navigate('Field Details');
      }}
      style={G.CARDLIST}>
      <ImageBackground
        source={{uri: item.fieldPhotos[0].photoUrl}}
        style={G.IMAGELIST}
        imageStyle={{borderRadius: 10}}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 0, y: 0}}
          colors={['#222', 'transparent']}
          style={G.CONTGRADIENT}></LinearGradient>
      </ImageBackground>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text numberOfLines={3} style={G.CAROUSELTXTSMALL}>
          {item.name}
        </Text>
        <Text numberOfLines={3} style={G.CAROUSELTXTSMALL}>
          Rp {Rupiah(item.price)}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={G.CONTAINER}>
      <View style={styles.MARGINHIGH}>
        <View style={styles.TEXTINPUT}>
          <TextInput
            value={find}
            onChangeText={(text) => setFind(text)}
            onSubmitEditing={() => search()}
            style={G.FORMINPUT}
            placeholderTextColor={'#ddd'}
            placeholder="Search by Location"
          />
        </View>
        {find != null ? (
          <FlatList
            data={data}
            renderItem={renderItem}
            numColumns={1}
            keyExtractor={keyExtractor}
            style={styles.CONTAINER}
            style={styles.FLATLIST}
            showsVerticalScrollIndicator={false}
          />
        ) : (
          <View style={styles.SEARCH}>
            <Text style={G.LABELFONTBOOK}>Lets Find Something</Text>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  MARGINHIGH:{
    marginTop:hp(2)
  },
  TEXTINPUT: {
    marginBottom: hp(2),
  },
  ROW: {
    flex: 1,
    justifyContent: 'space-around',
  },
  CONTAINER: {
    flexDirection: 'column',
    width: wp(90),
  },
  FLATLIST: {
    height: hp(100),
    marginBottom: hp(10),
  },
  SEARCH: {
    justifyContent: 'center',
    alignSelf: 'center',
    height: hp(50),
  },
});
