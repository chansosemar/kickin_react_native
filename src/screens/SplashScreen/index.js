import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import * as G from '../../config/style.js';
import {wp, fp, hp} from '../../utils/Responsive';
import Logo from '../../components/atoms/Logo'

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function SplashScreen() {
  return (
    <View style={[G.CONTENT, styles.CONTCONTENT]}>
      <Logo
            text='KICKIN'
            sizeLogo='LARGE'
            />
      <Text style={G.TITLEREVIEW}>Please Wait...</Text>
      <View style={styles.LOADING}>
        <ActivityIndicator size="small" color={color.light} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  CONTCONTENT: {
    position: 'relative',
    top: hp(35),
  },
  LOADING: {
    marginTop: hp(3),
  },
});
