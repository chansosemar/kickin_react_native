import React, {useState, useEffect} from 'react';
import {
	Text,
	StyleSheet,
	FlatList,
	View,
	Image,
	TouchableOpacity,
	ActivityIndicator,
	ImageBackground,
} from 'react-native';
import axios from 'axios';
import {Cover} from '../../config/images';
import * as G from '../../config/style.js';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {Rupiah} from '../../common/function/allFunction';
import {wp, fp, hp} from '../../utils/Responsive';
// SCREENS
import SplashScreen from '../SplashScreen';
import {HeaderNoBack} from '../components/HeaderComponent';
import Logo from '../../components/atoms/Logo';

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function HomePage(props) {
	// REDUX
	const field = useSelector((state) => state.field.data);
	const isLoading = useSelector((state) => state.field.isLoading);
	const dispatch = useDispatch();
	// HOOKS
	const [data, setData] = useState([]);
	const [name, setName] = useState([]);
	const [image, setImage] = useState([]);
	const [active, setActive] = useState('1');
	const navigation = useNavigation();
	const baseUrl = 'http://kickin.southeastasia.cloudapp.azure.com';
	console.log(field[0])
	// FUNCTION
	useEffect(() => {
		setData(field);
	}, [field]);

	// SORT BY

	function sortByName() {
		axios({
			method: 'GET',
			url: baseUrl + '/fields/sort/name',
		})
			.then((res) => {
				setData(res.data);
			})
			.catch((err) => {
				console.log('error', err);
			});
	}

	function sortByPrice() {
		axios({
			method: 'GET',
			url: baseUrl + '/fields/sort/price',
		})
			.then((res) => {
				setData(res.data);
			})
			.catch((err) => {
				console.log('error', err);
			});
	}

	// FLATLIST RENDER
	const keyExtractor = (item) => item.id;
	const renderItem = ({item}) => (
		<TouchableOpacity
			onPress={() => {
				dispatch({type: 'GET_FIELD_DETAIL', payload: item.id});
				navigation.navigate('Field Details');
			}}
			style={G.CARDLIST}>
			<ImageBackground
				source={{uri: item.fieldPhotos[0].photoUrl}}
				style={G.IMAGELIST}
				imageStyle={{borderRadius: 10}}>
				<LinearGradient
					start={{x: 0, y: 1}}
					end={{x: 0, y: 0}}
					colors={['#222', 'transparent']}
					style={G.CONTGRADIENT}></LinearGradient>
			</ImageBackground>
			<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
				<Text numberOfLines={3} style={G.CAROUSELTXTSMALL}>
					{item.name}
				</Text>
				<Text numberOfLines={3} style={G.CAROUSELTXTSMALL}>
					Rp {Rupiah(item.price)}
				</Text>
			</View>
		</TouchableOpacity>
	);

	return (
		<View style={G.CONTAINER}>
			<View style={{alignItems: 'center'}}>
				{isLoading ? (
					<SplashScreen />
				) : (
					<>
						<View style={styles.MARGINHIGH}>
							<Logo text="KICKIN" sizeLogo="LARGE" />
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<TouchableOpacity
								onPress={() => {
									dispatch({type: 'GET_FIELD'});
									sortByName(), setActive(1);
								}}>
								<Text style={active == 1 ? G.TXTSORTACTIVEBTN : G.TXTSORTBTN}>
									All
								</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => {
									sortByPrice(), setActive(2);
								}}>
								<Text style={active == 2 ? G.TXTSORTACTIVEBTN : G.TXTSORTBTN}>
									Price
								</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => {
									sortByName(), setActive(3);
								}}>
								<Text style={active == 3 ? G.TXTSORTACTIVEBTN : G.TXTSORTBTN}>
									Name
								</Text>
							</TouchableOpacity>
						</View>
						<FlatList
							data={data}
							renderItem={renderItem}
							numColumns={1}
							keyExtractor={keyExtractor}
							style={styles.CONTAINER}
							style={styles.FLATLIST}
							showsVerticalScrollIndicator={false}
						/>
					</>
				)}
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	ROW: {
		flex: 1,
		justifyContent: 'space-around',
	},
	CONTAINER: {
		flexDirection: 'column',
		width: wp(90),
	},
	FLATLIST: {
		height: hp(100),
		marginBottom: hp(10),
	},
	MARGINHIGH: {
		marginVertical: hp(6),
		alignSelf: 'center',
	},
});


