import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import * as G from '../../config/style.js';
import {wp, fp, hp} from '../../utils/Responsive';
import {useSelector, useDispatch} from 'react-redux';
import NotLoggedIn from '../../components/molecules/NotLoggedIn';
import NotBookingYet from '../../components/molecules/NotBookingYet';
import Review from '../../components/atoms/Review';

export default function ReviewPage({navigation}) {
	// REDUX
	const statusLogin = useSelector((state) => state.auth.isLoggedIn);
	const getUserReview = useSelector((state) => state.getUserReview.data);

	return (
		<View style={G.CONTAINER}>
			<Text style={G.HISTORYTITLE}>Your Review</Text>
			{!getUserReview && statusLogin ? (
				<NotBookingYet />
			) : statusLogin ? (
				getUserReview.map((items, index) => (
					<Review items={items} key={items.id} />
				))
			) : (
				<NotLoggedIn navigation={navigation} />
			)}
		</View>
	);
}

const styles = StyleSheet.create({});
