import React from 'react';
import {Text, Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

function Header(props) {
  const navigation = useNavigation();

  return (
    <View style={styles.headerPosition}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <Icon name="arrow-left" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Search')}>
        <Icon name="search" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

export function HeaderNoBack(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.headerPositionNoBack}>
      <TouchableOpacity onPress={() => navigation.navigate('Search')}>
        <Icon name="search" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

export function HeaderBack(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.headerPositionBack}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <Icon name="arrow-left" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

export function HeaderBackRental(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.headerPositionBack}>
      <TouchableOpacity onPress={() => navigation.navigate('Book')}>
        <Icon name="arrow-left" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

export function HeaderCheckout(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.headerPositionBack}>
      <TouchableOpacity>
        <Icon name="arrow-left" size={20} color={'#fff'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

function HeaderClose(props) {
  return (
    <View style={styles.headerPositionNoBack}>
      <TouchableOpacity>
        <Icon name="close" size={20} color={'#d72222'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  headerPosition: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  headerPositionNoBack: {
    alignItems: 'flex-end',
    padding: 10,
  },
  headerPositionBack: {
    alignItems: 'flex-start',
    padding: 10,
    backgroundColor:'#222'
  },
  icon: {
    backgroundColor: 'rgba(34,34,34,0.5)',
    padding: 15,
    borderRadius: 50,
  },
});
