import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import * as G from '../../constant/style.js';
import {Rating} from 'react-native-elements';
import {useSelector} from 'react-redux';
import {Rupiah} from '../../common/function/allFunction';

export default function DescriptionComponent(props) {
  // REDUX
  const fieldDetail = useSelector((state) => state.fieldDetail.data);

  return (
    <>
      <View style={G.FLEXCONT}>
        <Text style={[G.BOLDXLFONT, styles.COLUMN]}>
          {fieldDetail.name}
        </Text>
        <Text style={[G.BOLDFONT, styles.PRICE]}>
          Rp {Rupiah(fieldDetail.price)}
        </Text>
      </View>
      <View style={G.FLEXCONT}>
        <Text style={[G.THINXLFONT, styles.COLUMN]}>
          {fieldDetail.address}
        </Text>
        <TouchableOpacity
          onPress={() => Linking.openURL(fieldDetail.addressUrl)}>
          <Text style={[G.BOLDFONT, styles.DIRECTION]}>Direction</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.CONTDESC}>
        <Text style={[G.BOLDFONT, styles.TITLE]}>Description</Text>
        <Text style={G.REGFONT}>{fieldDetail.description}</Text>
        <Text style={G.REGFONT}>{fieldDetail.review}</Text>
        <Text style={[G.BOLDFONT, styles.TITLE]}>Feedback and Review</Text>
        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <Rating
            imageSize={20}
            readonly
            startingValue={fieldDetail.average}
            tintColor={'#222'}
            style={{marginRight: 10}}
          />
          <Text style={G.MEDFONT}>{Math.floor(fieldDetail.average)} / 5</Text>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  COLUMN: {
    color: '#fff',
    flex: 1,
    paddingRight: 20,
  },
  PRICE: {
    color: '#fff',
    top: 5,
    flex: 1,
    textAlign: 'right',
  },
  DIRECTION: {
    color: '#53C9C2',
    flex: 1,
    textAlign: 'right',
  },
  TITLE: {
    color: '#fff',
    marginVertical: 10,
  },
  CONTDESC: {
    width: '100%',
    marginTop: 20,
  },
});
