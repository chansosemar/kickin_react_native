import React, {useRef, useEffect, useState} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as GLOBAL from '../../constant';
import * as G from '../../constant/style.js';
import {Rating} from 'react-native-elements';
import {useSelector} from 'react-redux';

export default function ReviewComponent(props) {
  // REDUX
  const fieldDetail = useSelector((state) => state.fieldDetail.data);
  return (
    <>
      {fieldDetail.reviews.map((items, index) => (
        <View style={styles.CONTREVIEW} key={items.user.fullname}>
          <View style={G.CARDREVIEW}>
            <Text style={G.TITLEREVIEW}>{items.user.fullname}</Text>
            <View style={styles.RATING}>
              <Rating
                imageSize={20}
                readonly
                startingValue={items.rating}
                tintColor={'#222'}
              />
            </View>
            <Text style={G.TXTREVIEW}>{items.comment}</Text>
          </View>
        </View>
      ))}
    </>
  );
}

const styles = StyleSheet.create({
  RATING: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  CONTREVIEW: {
    width: '100%',
  },
});
