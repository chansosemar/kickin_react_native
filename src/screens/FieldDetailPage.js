import React, {useEffect, useState, useRef} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Linking,
  ScrollView,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import * as G from '../constant/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import Carousel from 'react-native-snap-carousel';
import {useSelector, useDispatch} from 'react-redux';
import axios from 'axios';
import {Rating} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
// SCREENS
import Header from './components/HeaderComponent';
import DescriptionComponent from './components/DescriptionComponent';
import ReviewComponent from './components/ReviewComponent';
import SplashScreen from './SplashScreen';

export default function FieldDetailPage(props) {
  // REDUX
  const review = useSelector((state) => state.review.data);
  const isLoading = useSelector((state) => state.fieldDetail.isLoading);
  const fieldDetail = useSelector((state) => state.fieldDetail.data);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const dispatch = useDispatch();

  // HOOKS
  const [activeIndex, setActiveIndex] = useState(0);
  const [image, setImage] = useState([]);
  const [bigImage, setBigImage] = useState();

  // FUNCTION
  const navigation = useNavigation();

  useEffect(() => {
    setImage(fieldDetail.fieldPhotos);
    if (fieldDetail.fieldPhotos.length > 0) {
      setBigImage(fieldDetail.fieldPhotos[0].photoUrl);
    }
  }, [fieldDetail]);
  
  return (
    <View style={G.CONTAINER}>
      {isLoading ? (
        <SplashScreen />
      ) : (
        <>
          <ImageBackground
            source={
              bigImage ? {uri: bigImage} : require('../../assets/COVER.png')
            }
            style={G.FIELDDETAILIMG}>
            <LinearGradient
              start={{x: 0, y: 1}}
              end={{x: 0, y: 0}}
              colors={['#222', 'transparent']}
              style={styles.CONTAINER}>
              <Header />
            </LinearGradient>
          </ImageBackground>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            {fieldDetail.fieldPhotos.map((item, index) => (
              <TouchableOpacity
                onPress={() => setBigImage(item.photoUrl)}
                key={item.id}>
                <Image
                  source={{uri: item.photoUrl}}
                  style={{
                    width: (G.DIMENSWIDTH * 0.8) / 5,
                    height: 60,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}
                />
              </TouchableOpacity>
            ))}
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.SCROLLVIEW}>
            <View style={G.CONTENTFIELDDETAIL}>
              <DescriptionComponent />
              <ReviewComponent />
            </View>
          </ScrollView>
          <TouchableOpacity
            onPress={() => {isLoggedIn ?
              (dispatch({type: 'GET_BOOKING_FIELD', payload: fieldDetail.id}),
                            navigation.navigate('Book', {item: fieldDetail})): 
              navigation.navigate('Login');
            }}
            style={[G.FIELDDETAILBTN, styles.BTNPOST]}>
            <Text style={G.TXTMAINBTN}>Book</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  direction: {
    color: '#53C9C2',
    flex: 1,
    textAlign: 'right',
  },
  BTNPOST: {
    position: 'absolute',
    bottom: 10,
  },
  SCROLLVIEW: {
    marginTop: 10,
    marginBottom: 80,
  },
  CONTAINER: {
    position: 'absolute',
    width: '100%',
    height: 200,
  },
});
