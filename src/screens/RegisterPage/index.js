import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import * as G from '../../config/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';
import {GoogleSignin} from '@react-native-community/google-signin';
import {onGoogleButtonPress} from '../../common/function/authGoogle';
import auth from '@react-native-firebase/auth';
import {wp, fp, hp} from '../../utils/Responsive';
import {AppStyle} from '../../config/style';
// ATOMS
import Button from '../../components/atoms/Button';

const {fontSize, fontColor, font, color} = AppStyle;

export default function RegisterPage({navigation}) {
  // REDUX
  const dispatch = useDispatch()
  const statusRegister = useSelector((state) => state.auth.statusRegister)

  // HOOKS
  const [fullname, setFullname] = useState(null);
  const [email, setEmail] = useState(null);
  const [phone, setPhone] = useState(null);
  const [password, setPassword] = useState(null);
  const [message, setMessage] = useState(null);
  const [hide, setHide] = useState(true);
  const [googleToken, setGoogleToken] = useState();

  GoogleSignin.configure({
    webClientId: '257695633255-8ut398jnnq4f4s5pj99irv46mishs6i2',
  });

  async function onGoogleButtonPress() {
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    setGoogleToken(googleCredential.token);
    const data = {
      token : googleCredential.token
    };
    dispatch({type:'REGISTER_GOOGLE', payload:data})
    return auth().signInWithCredential(googleCredential);
  }

  console.log('inigoogle>>', googleToken);
  // Show and Hide Password
  const show = () => {
    setHide(false);
    setTimeout(() => {
      setHide(true);
    }, 1000);
  };

  // Password and Email formatting
  const submit = () => {
    let mailformat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    let passwordformat = /^(?=.*[0-9])(?=.*[A-Z]).{8,32}$/g;

    if (!fullname) {
      setMessage('Full Name Must be Field !!');
    } else if (!email) {
      setMessage('Email Must be Field !!');
    } else if (!email.match(mailformat)) {
      setMessage('Email Invalid !!');
    } else if (!phone) {
      setMessage('Phone Number Must be Field !!');
    } else if (!password) {
      setMessage('Password Must be Field !!');
    } else if (!password.match(passwordformat)) {
      setMessage('Password Invalid !!');
    } else {
      const dataRegister = {
        fullname: fullname,
        email: email,
        phoneNumber: phone,
        password: password,
      };
      dispatch({type:'REGISTER', payload:dataRegister})
    }
  };

  // Callback to loginpage
  useEffect(() => {
    if (statusRegister == true) {
      navigation.navigate('Login');
    }
  }, [statusRegister]);

  // Keyboard Setting
  const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0;

  return (
    <View style={G.CONTAINER}>
      <View style={G.CONTENT}>
        <KeyboardAvoidingView
          behavior="position"
          keyboardVerticalOffset={keyboardVerticalOffset}>
          <View style={styles.TEXTINPUT}>
            <Text style={G.WARNTEXT}>{message}</Text>
            <Text style={G.LABELFONT}>Full Name</Text>
            <TextInput
              value={fullname}
              onChangeText={(text) => setFullname(text)}
              style={G.FORMINPUT}
            />
            <Text style={G.LABELFONT}>Email</Text>
            <TextInput
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={G.FORMINPUT}
            />
            <Text style={G.LABELFONT}>Phone Number</Text>
            <TextInput
              value={phone}
              onChangeText={(text) => setPhone(text)}
              style={G.FORMINPUT}
            />
            <Text style={G.LABELFONT}>Password </Text>
            <Text style={G.THINFONT}>
              (Contain at least one Number, one Uppercase and 8 character long)
            </Text>
            <View>
              <TextInput
                value={password}
                onChangeText={(text) => setPassword(text)}
                style={G.FORMINPUT}
                secureTextEntry={hide}
              />
              <TouchableOpacity onPress={() => show()} style={G.SHOWPASSWORD}>
                <Icon name="eye" size={hp(3)} color={color.light} />
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
        <Button
            text="Sign Up"
            typeButton="PRIMARY"
            onPress={() => submit()}
          />
          <Button
            text="Sign Up with Google"
            typeButton="GOOGLE"
            onPress={() => onGoogleButtonPress()}
          />
        <View style={styles.MARGINLOGIN}>
          <Button
            text="Already have an account? Log In"
            typeButton="TEXT"
            onPress={() => navigation.navigate('Login')}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  TEXTINPUT: {
    marginVertical: hp(1),
  },
  MARGINLOGIN: {
    marginVertical: hp(2),
    alignSelf: 'center',
  },
});
