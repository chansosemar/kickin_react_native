import React, {useEffect} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import * as G from '../constant/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

export default function ReceiptUploadPage() {
  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <View style={G.CONTAINER}>
      <View style={G.CONTENTCHECK}>
        <View style={{marginVertical: 5}}>
          <Text style={[G.HEADERFONTCHECK, styles.MARGIN]}>Transfer</Text>
          <View style={styles.LINE}></View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONT, styles.FLEX]}>Name</Text>
            <Text style={[G.LABELFONT, styles.FLEX]}>PT. KICKIN MAJU JAYA</Text>
          </View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONT, styles.FLEX]}>No. Account</Text>
            <Text style={[G.LABELFONT, styles.FLEX]}>525533671</Text>
          </View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text style={[G.LABELFONTCHECK, styles.FLEX]}>Total</Text>
            <Text style={[G.LABELFONTCHECK, styles.FLEX]}>Rp 110.000</Text>
          </View>
        </View>
        <View style={styles.LINE}></View>
        <View style={{marginVertical: 5}}>
          <Text style={[G.LABELFONTCHECK, styles.MARGIN]}>UPLOAD RECEIPT</Text>
          <View style={styles.MARGIN}></View>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#53C9C2',
                borderWidth: 1,
                padding: 10,
                borderRadius: 10,
              }}>
              <TouchableOpacity
                style={{
                  width: 150,
                  height: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#313131',
                  padding: 10,
                  borderRadius: 10,
                }}>
                <Text>
                  {' '}
                  <Icon name="camera" size={20} color={'#53C9C2'} />{' '}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.FLEX}>
              <Text style={[G.SPCTXT, styles.CENTER]}>
                After upload your receipt, wait for max 24 Hours. Please Check
                your History Tab Frequently
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.MARGIN}></View>
        <TouchableOpacity style={G.MAINBTN}>
          <Text style={G.TXTMAINBTN}>Submit</Text>
        </TouchableOpacity>
        <TouchableOpacity style={G.GOOGLEBTN}>
          <Text style={G.TXTLATERBTN}>Later</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  FLEX: {
    flex: 1,
  },
  MARGIN: {
    marginVertical: 5,
  },
  LINE: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#fff',
    marginVertical: 10,
  },
  headerPositionBack: {
    alignItems: 'flex-end',
    padding: 10,
    backgroundColor: '#222',
  },
  SETSPCTXT: {
    marginVertical: 20,
  },
  CENTER: {
    textAlign: 'center',
    padding: 5,
  },
});

function HeaderClose(props) {
  return (
    <View style={styles.headerPositionBack}>
      <TouchableOpacity>
        <Icon name="close" size={20} color={'#d72222'} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}
