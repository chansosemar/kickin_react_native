import React, {useState, useEffect} from 'react';
import {
	Text,
	StyleSheet,
	View,
	Image,
	TouchableOpacity,
	ScrollView,
} from 'react-native';
import * as G from '../../config/style.js';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {Rupiah} from '../../common/function/allFunction';
import {wp, fp, hp} from '../../utils/Responsive';
import moment from 'moment';
// SCREENS
import PaymentAdmin from '../../components/atoms/PaymentAdmin';
import BookingAdmin from '../../components/atoms/BookingAdmin';
import SplashScreen from '../SplashScreen';
import Logo from '../../components/atoms/Logo';

const {fontSize, fontColor, font, color} = G.AppStyle;

export default function HomePage(props) {
	// REDUX
	const isLoading = useSelector((state) => state.getPaymentAdmin.isLoading);
	const payment = useSelector(
		(state) => state.getPaymentAdmin.data.getpaymentListToday,
	);
	const booking = useSelector(
		(state) => state.getBookingAdmin.data.getBookingListToday,
	);
	const dispatch = useDispatch();
	// HOOKS
	const [dataPayment, setDataPayment] = useState([]);
	const [dataBooking, setDataBooking] = useState([]);
	const [active, setActive] = useState('1');
	const [progress, setProgress] = useState('waiting');
	const navigation = useNavigation();

	// FUNCTION
	useEffect(() => {
		if (payment != null) {
			setDataPayment(payment);
		}
		if (booking != null) {
			setDataBooking(booking);
		}
	}, [payment, booking]);


	return (
		<View style={G.CONTAINER}>
			<View style={{alignItems: 'center'}}>
				{isLoading ? (
					<SplashScreen />
				) : (
					<>
						<View style={styles.MARGINHIGH}>
							<TouchableOpacity
								onPress={() => dispatch({type: 'LOGOUT_ADMIN'})}>
								<Logo text="ADMIN PAGE" sizeLogo="LARGE" />
							</TouchableOpacity>
						</View>
						<View style={styles.FLEX}>
							<Text
								style={[active == 1 ? G.HISTORYTITLEACTIVE : G.HISTORYTITLE]}
								onPress={() => {
									dispatch({type:'GET_PAYADM'})
									setActive(1)}}>
								Payment
							</Text>
							<Text
								style={[active == 2 ? G.HISTORYTITLEACTIVE : G.HISTORYTITLE]}
								onPress={() => {
									dispatch({type:'GET_BOOKADM'})
									setActive(2)}}>
								Booking
							</Text>
						</View>
						<ScrollView>
							{active == 1
								? dataPayment.map((items, index) => (
										<PaymentAdmin items={items} key={items.createdAt}/>
								  ))
								: dataBooking.map((items, index) => (
										<BookingAdmin items={items} key={items.createdAt} />
								  ))}
						</ScrollView>
					</>
				)}
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	MARGINHIGH: {
		marginVertical: hp(3),
	},

	FLEX: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		width: wp(90),
	},
});
