import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import * as G from '../constant/style.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import {connect, useDispatch, useSelector} from 'react-redux';
import {Rupiah} from '../common/function/allFunction';


export default function RentalKitPage(props) {
  const rentalList = useSelector((state) => state.getRentalKitList.data);
  const isLoading = useSelector((state) => state.getRentalKitList.isLoading);
  const bookingData = useSelector((state) => state.booking.data.booking);
  const dispatch = useDispatch()
  const navigation = useNavigation();

  const [socksQty, setSocksQty] = useState('');
  const [socksSize, setSocksSize] = useState('');
  const [shoesQty, setShoesQty] = useState('');
  const [shoesSize, setShoesSize] = useState('');
  const [vestQty, setVestQty] = useState('');
  const [vestSize, setVestSize] = useState('');

 
  const submit = () => {
    const dataSocks = {
      id: bookingData.id,
      size: socksSize,
      quantity: socksQty,
    }
    const dataShoes = {
      id: bookingData.id,
      size: shoesSize,
      quantity: shoesQty
    }
    const dataVest = {
      id: bookingData.id,
      size: vestSize,
      quantity: vestQty
    }
    dispatch({type : 'POST_ACTION', payload: {dataSocks,dataShoes,dataVest}});
    // dispatch({type : 'POST_SOCKS', payload: dataSocks});
    // dispatch({type : 'POST_SHOES', payload: dataShoes});
    if(!isLoading){
      props.navigation.navigate('Checkout')
    }
  }
  

  console.log(bookingData);

  return (
    <View style={G.CONTAINER}>
      <View style={G.CONTENTCHECK}>
        <View style={{marginVertical: 5}}>
          <Text style={[G.HEADERFONTCHECK, styles.MARGIN]}>
            Rental Kit List
          </Text>
          <View style={styles.LINE}></View>
          {rentalList.map((items, index) => (
            <View key={index}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={[G.LABELFONT, styles.FLEX]}>{items.name}</Text>
              </View>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <TextInput
                  style={G.FORMINPUTRENTAL}
                  placeholder="Qty"
                  textAlign={'center'}
                  placeholderTextColor={'#fff'}
                  keyboardType={'number-pad'}
                  onChangeText={(text) => {
                    if (items.name == 'Socks') {
                      setSocksQty(text);
                    } else if (items.name == 'Vest') {
                      setVestQty(text);
                    } else {
                      setShoesQty(text);
                    }
                  }}
                />
                <TextInput
                  style={G.FORMINPUTRENTAL}
                  placeholder="Size"
                  textAlign={'center'}
                  keyboardType={
                    items.name == 'Shoes' ? 'number-pad' : 'default'
                  }
                  placeholderTextColor={'#fff'}
                  onChangeText={(text) => {
                    if (items.name == 'Socks') {
                      setSocksSize(text);
                    } else if (items.name == 'Vest') {
                      setVestSize(text);
                    } else {
                      setShoesSize(text);
                    }
                  }}
                />
                <Text style={[G.LABELFONT, styles.ALIGN]}>
                  Rp{' '}
                  {items.name == 'Socks'
                    ? Rupiah(items.price * socksQty)
                    : items.name == 'Vest'
                    ? Rupiah(items.price * vestQty)
                    : Rupiah(items.price * shoesQty)}
                </Text>
              </View>
            </View>
          ))}
        </View>
        <View style={styles.LINE}></View>
        <View style={styles.MARGIN}></View>
        <TouchableOpacity 
          onPress={() => submit()}
          style={G.MAINBTN}>
          <Text style={G.TXTMAINBTN}>Submit</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          onPress={() => submit()}
          style={G.SKIPBTN}>
          <Text style={G.TXTSNDBTN}>Skip</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ALIGN: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  FLEX: {
    flex: 1,
  },
  MARGIN: {
    marginVertical: 5,
  },
  LINE: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#fff',
    marginVertical: 10,
  },
  SETSPCTXT: {
    marginVertical: 20,
  },
  CENTER: {
    textAlign: 'center',
    padding: 5,
  },
});

