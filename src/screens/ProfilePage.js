import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  ScrollView,
  Modal,
  TextInput,
} from 'react-native';
import * as G from '../constant/style.js';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Rating} from 'react-native-elements';
// SCREENS
import {HeaderNoBack} from './components/HeaderComponent';

export default function ProfilePage(props) {
  // REDUX
  const profile = useSelector((state) => state.profile.data[0]);
  const statusLogin = useSelector((state) => state.auth.isLoggedIn);
  const getUserReview = useSelector((state) => state.getUserReview.data);
  const dispatch = useDispatch();

  // HOOKS
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [resRating, setResRating] = useState();
  const [comment, setComment] = useState();

  // FUNCTION
  // RATING
  const submit = () => {
    const data = {
      id: getUserReview[0].id,
      rating: resRating,
      comment: comment,
    };
    dispatch({type: 'EDIT_REVIEW', payload: data});
    setModalVisible(false);
  };

  return (
    <>
      {statusLogin ? (
        <View style={{backgroundColor: '#222'}}>
          {profile.photo == null ? (
            <ImageBackground
              source={require('../../assets/user.png')}
              style={G.PROFILEIMG}>
              <LinearGradient
                start={{x: 0, y: 1}}
                end={{x: 0, y: 0}}
                colors={['#222', 'transparent']}
                style={G.PROFILEGRADIENT}></LinearGradient>
            </ImageBackground>
          ) : (
            <ImageBackground source={{uri: profile.photo}} style={G.PROFILEIMG}>
              <LinearGradient
                start={{x: 0, y: 1}}
                end={{x: 0, y: 0}}
                colors={['#222', 'transparent']}
                style={G.PROFILEGRADIENT}></LinearGradient>
            </ImageBackground>
          )}

          <View style={G.CONTAINER}>
            <View style={G.CONTENTPROFILE}>
              
              <ScrollView
                style={{marginBottom: 300}}
                showsVerticalScrollIndicator={false}>
                <View style={styles.LIST}>
                  <Text style={G.LABELFONTPROFILE}>Name</Text>
                  <Text style={G.REGFONTPROFILE}>{profile.fullname}</Text>
                </View>
                <View style={styles.LIST}>
                  <Text style={G.LABELFONTPROFILE}>Email</Text>
                  <Text style={G.REGFONTPROFILE}>{profile.email}</Text>
                </View>
                <View style={styles.LIST}>
                  <Text style={G.LABELFONTPROFILE}>Phone</Text>
                  <Text style={G.REGFONTPROFILE}>
                    {profile.phoneNumber == null ? '-' : profile.phoneNumber}
                  </Text>
                </View>
                <View style={{width: G.DIMENSWIDTH * 0.9, marginVertical: 10}}>
                  <Text style={G.LABELFONTPROFILE}>Bio</Text>
                  {profile.bio == null ? (
                    <Text style={[G.REGFONTPROFILE, styles.BIO]}>
                      - Lengkapi Bio anda Melalui Edit Profile -
                    </Text>
                  ) : (
                    <Text style={[G.REGFONTPROFILE, styles.BIO]}>
                      {profile.bio}
                    </Text>
                  )}
                </View>
                <View style={styles.BTNWRAPPER}>
                <TouchableOpacity
                  style={G.LGTBTN}
                  onPress={() => dispatch({type: 'LOGOUT'})}>
                  <Text style={G.TXTLGTBTN}>Log Out</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={G.EDITBTN}
                  onPress={() => navigation.navigate('Edit Profile')}>
                  <Text style={G.TXTEDITBTN}>Edit Profile</Text>
                </TouchableOpacity>
              </View>
              </ScrollView>

            </View>
          </View>
        </View>
      ) : (
        <View style={{backgroundColor: '#222'}}>
          <View style={styles.CONTPOSITION}>
            <View style={styles.TEXT}>
              <Text style={[G.LABELFONT, styles.BLUEFONT]}>
                You are not Logged In
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate('Login')}
              style={G.MAINBTN}>
              <Text style={G.TXTMAINBTN}>Login</Text>
            </TouchableOpacity>

            <View style={styles.TEXT}>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <Text style={[G.LABELFONT, styles.BLUEFONT]}>
                  Don't have an account? Sign Up
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  LIST: {
    width: G.DIMENSWIDTH * 0.9,
    flexDirection: 'row',
    marginVertical: 5,
  },
  BIO: {
    alignSelf: 'center',
    textAlign: 'justify',
    fontSize: 16,
    marginVertical: 10,
  },
  TEXT: {
    marginVertical: 20,
    alignSelf: 'center',
  },
  CONTPOSITION: {
    position: 'relative',
    top: G.DIMENSHEIGHT * 0.25,
    height: G.DIMENSHEIGHT,
    alignSelf: 'center',
  },
  CONTREVIEW: {
    width: '100%',
  },
  RATING: {
    flexDirection: 'row',
  },
  COMPONENT: {
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  ICON: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  MODALCENTER: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  BLUEFONT: {
    color: '#53C9C2',
  },
  BTNWRAPPER: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginBottom: 20,
  },
});
