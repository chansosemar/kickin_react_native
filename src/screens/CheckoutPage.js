import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import * as G from '../constant/style.js';
import {useNavigation} from '@react-navigation/native';
import {HeaderCheckout} from './components/HeaderComponent';
import {connect, useDispatch, useSelector} from 'react-redux';
import {
	convertDateToString,
	convertTimeToString,
	Rupiah,
} from '../common/function/allFunction';
import SplashScreen from './SplashScreen';
import moment from 'moment-timezone';

export default function CheckoutPage(props) {
	const bookingData = useSelector((state) => state.booking.data.booking);
	const fieldData = useSelector((state) => state.fieldDetail.data);
	const listItem = useSelector((state) => state.getRentalKitList.data);
	const dispatch = useDispatch();
	const navigation = useNavigation();

	// CHECK RENTAL KIT LIST
	const vestId = {id: '1e31d25a-d9f0-4905-bbcd-df75873c5965', price: '10000'};
	const shoesId = {id: '47302d5a-12c3-4f32-8cd5-56e5b95daf50', price: '15000'};
	const socksId = {id: '28842a19-b57b-4bcc-ba8c-6ccd64406c34', price: '5000'};
	const [vestData, setVestData] = useState();
	const [shoesData, setShoesData] = useState();
	const [socksData, setSocksData] = useState();
	// const [rentalPrice, setRentalPrice] = useState()

	// FORMAT TIME TO LOCALE
	const [time, setTime] = useState(bookingData.startTime);
	const formatTime = moment(time);
	let convertedTime = formatTime.tz('Europe/London').format('LT');
	console.log(bookingData)
	// GET DATA LIST ITEM
	useEffect(() => {
		let findVest = listItem.find(function (e) {
			if (e.rentItemId == vestId.id) {
				setVestData(e);
			}
		});
		let findShoes = listItem.find(function (e) {
			if (e.rentItemId == shoesId.id) {
				setShoesData(e);
			}
		});
		let findSocks = listItem.find(function (e) {
			if (e.rentItemId == socksId.id) {
				setSocksData(e);
			}
		});
	}, [listItem]);

	// COUNT DURATION & PRICE
	const temp = [];
	const convertStart = convertTimeToString(bookingData.startTime);
	const convertEnd = convertTimeToString(bookingData.endTime);
	const awal = convertStart.split(':');
	const akhir = convertEnd.split(':');
	temp.push(awal[0]);
	temp.push(akhir[0]);
	const duration = temp[1] - temp[0];
	const fieldPrice = fieldData.price * duration;
	const totalPrice = fieldPrice;

	// SUBMIT DATA
	const submit = () => {
		const data = {
			id: bookingData.id,
		};
		dispatch({type: 'POST_TRX', payload: data});
		dispatch({type: 'GET_TRX', payload: data.id})
		props.navigation.navigate('Upload Receipt');
	};

	return (
		<View style={G.CONTAINER}>
			{vestData == null || socksData == null || shoesData == null ? (
				<SplashScreen />
			) : (
				<View style={G.CONTENTCHECK}>
					<View style={{marginVertical: 5}}>
						<Text style={[G.HEADERFONTCHECK, styles.MARGIN]}>Review</Text>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Field Name</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>{fieldData.name}</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Address</Text>
							<Text style={[G.LABELFONT, styles.FLEX]} numberOfLines={3}>
								{fieldData.address}
							</Text>
						</View>
					</View>
					<View style={{marginVertical: 5}}>
						<Text style={[G.LABELFONTCHECK, styles.MARGIN]}>Cart</Text>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Date</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								{convertDateToString(bookingData.startTime)}
							</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Time</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>{convertedTime}</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Duration</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>{duration} Hour(s)</Text>
						</View>
					</View>
					<View style={{marginVertical: 5}}>
						<Text style={[G.LABELFONTCHECK, styles.MARGIN]}>Rental Kit</Text>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Shocks</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								{socksData.quantity} pcs / Size {socksData.size}
							</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Shoes</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								{shoesData.quantity} pcs / Size {shoesData.size}
							</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Vest</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								{vestData.quantity} pcs / Size {vestData.size}
							</Text>
						</View>
					</View>
					<View style={styles.LINE}></View>
					<View style={{marginVertical: 5}}>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Field</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								Rp {Rupiah(fieldPrice)}
							</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONT, styles.FLEX]}>Rental Kit</Text>
							<Text style={[G.LABELFONT, styles.FLEX]}>
								Rp{' '}
								{Rupiah(
									vestData.quantity * vestId.price +
										shoesData.quantity * shoesId.price +
										socksData.quantity * socksId.price,
								)}
							</Text>
						</View>
						<View style={{flexDirection: 'row', width: '100%'}}>
							<Text style={[G.LABELFONTCHECK, styles.FLEX]}>Total</Text>
							<Text style={[G.LABELFONTCHECK, styles.FLEX]}>
								Rp{' '}
								{Rupiah(
									fieldPrice +
										vestData.quantity * vestId.price +
										shoesData.quantity * shoesId.price +
										socksData.quantity * socksId.price,
								)}
							</Text>
						</View>
					</View>
					<View style={styles.LINE}></View>
					<TouchableOpacity onPress={() => submit()} style={G.MAINBTN}>
						<Text style={G.TXTMAINBTN}>Pay</Text>
					</TouchableOpacity>
					<View style={styles.SETSPCTXT}>
						<Text style={G.SPCTXT}>
							After click Pay, those items can't be change anymore. Please check
							your Review Properly
						</Text>
					</View>
				</View>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	FLEX: {
		flex: 1,
	},
	MARGIN: {
		marginVertical: 5,
	},
	LINE: {
		width: '100%',
		borderTopWidth: 1,
		borderTopColor: '#fff',
		marginVertical: 10,
	},
	SETSPCTXT: {
		marginVertical: 20,
	},
});
