import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import * as GLOBAL from '../constant';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {addTicketAction} from '../redux/action/ticketAction';

function AfterBook(props) {
  const navigation = useNavigation();
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(props.booking);
  }, [props.booking]);

  console.log('ini booking data', data.id);
  const submit = () => {
    props.addTicket(data.id);
    navigation.navigate('History');
  };

  return (
    <View style={GLOBAL.CONTAINER}>
      <View style={[GLOBAL.CONTENT, styles.posContent]}>
        <Text style={[GLOBAL.LABELFONT]}> Hello {props.profile.fullname}</Text>
        <Text style={[GLOBAL.BOLDFONT, styles.whiteText]}>
          {' '}
          Congrats your field already BOOK !!
        </Text>
        <Text style={[GLOBAL.BOLDFONT, styles.whiteText]}>
          Check Your Ticket On HISTORY Tab
        </Text>
        <TouchableOpacity onPress={() => submit()} style={GLOBAL.BOOKBTN}>
          <Text style={GLOBAL.TXTMAINBUTTON}>Add Ticket</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
('');

const mapStateToProps = (state) => ({
  profile: state.profile.data[0],
  booking: state.booking.data.data.booking,
});

const mapDispatchToProps = (dispatch) => ({
  addTicket: (data) => dispatch(addTicketAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AfterBook);

const styles = StyleSheet.create({
  whiteText: {
    color: '#fff',
    marginVertical: 5,
  },
  posContent: {
    position: 'absolute',
    top: GLOBAL.DIMENSHEIGHT * 0.4,
  },
});
