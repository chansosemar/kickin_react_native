import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
const baseUrl = 'http://ec2-3-0-98-35.ap-southeast-1.compute.amazonaws.com';

export const convertDateToString = (selectedDate) => {
  selectedDate = new Date(selectedDate);
  const currDate = selectedDate.getDate();
  const currMonth = selectedDate.getMonth() + 1;
  const currYear = selectedDate.getFullYear();
  return (
   currDate + ' ' + '-' + ' ' + currMonth + ' ' + '-' + ' ' + currYear
  );
};

export const convertTimeToString = (selectedTime) => {
  selectedTime = new Date(selectedTime);
  const currHour = ('0' + selectedTime.getHours()).slice(-2);
  const currMinute = ('0' + selectedTime.getMinutes()).slice(-2);
  return currHour + ':' + currMinute;
};

export const Rupiah = (price) => {
  let priceString = `${price}`,
    rest = priceString.length % 3,
    rp = priceString.substr(0, rest),
    thousand = priceString.substr(rest).match(/\d{3}/g);

  if (thousand) {
    let separator = rest ? '.' : '';
    rp += separator + thousand.join('.');
  }
  return rp;
};

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export function pickImage() {
  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = {
        uri: response.uri,
        type: response.type,
        name: response.fileName,
        data: response.data,
      };

      setRawImage(source);
      setImage(response.uri);
    }
  });
}

export async function uploadImage() {
  const token = await AsyncStorage.getItem('APP_AUTH_TOKEN');
  const data = new FormData();
  data.append('file', rawImage);

  axios({
    method: 'POST',
    url: baseUrl + '/files',
    data: data,
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
      'Content-Type': 'multipart/form-data',
    },
  })
    .then((res) => {
      console.log('res', res.data);
      alert('Berhasil Upload !!');
      console.log(res.data.url);
      setImage(res.data);
    })
    .catch((err) => {
      console.log('error', err);
      alert('Gagal Upload');
    });
}

 export const checkDate = (b) => {
    let a = new Date();
    let B = new Date(b);
    let contA = a.setDate(a.getDate());
    let contB = B.setDate(B.getDate());
    let compare = contB - contA;
    if (compare < 0) {
      return true;
    }
    return false;
  };